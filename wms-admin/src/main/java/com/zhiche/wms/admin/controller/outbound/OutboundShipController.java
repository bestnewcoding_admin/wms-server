package com.zhiche.wms.admin.controller.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.outbound.*;
import com.zhiche.wms.service.outbound.IOutboundShipHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/outboundShipManage")
public class OutboundShipController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutboundShipController.class);

    @Autowired
    private IOutboundShipHeaderService shipHeaderService;

    @Autowired
    private IOutboundShipLineService shipLineService;

    /**
     * 出库管理列表查询
     */
    @PostMapping("/getOutboundShipList")
    public RestfulResponse<Page<OutboundShipListDTO>> getOutboundShipListNew(
            @RequestBody Page<OutboundShipListDTO> page) {
        Page<OutboundShipListDTO> shipList = shipHeaderService.queryOutboundShipPage(page);
        return new RestfulResponse<>(0, "查询成功", shipList);
    }

    /**
     * 出库确认 -- 支持批量出库
     */
    @PostMapping("/outboundShipConfirm")
    public RestfulResponse<List<String>> updateOutboundShipConfirm(@RequestBody OutboundShipParamDTO dto) {
        RestfulResponse<List<String>> restfulResponse = new RestfulResponse<>(0, "出库成功", null);
        try {
            ArrayList<String> data = shipHeaderService.updateOutboundShipConfirm(dto);
            if (CollectionUtils.isNotEmpty(data)) {
                restfulResponse.setCode(-2);
                restfulResponse.setMessage("处理结果");
                restfulResponse.setData(data);
            }
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 扫码获取车辆信息
     */
    @PostMapping("/getOutShipInfoScan")
    public RestfulResponse<OutboundShipQrCodeResultDTO> getOutShipInfoByScan(@RequestBody OutboundShipParamDTO dto) {
        OutboundShipQrCodeResultDTO info = shipHeaderService.getOutShipInfoByScan(dto);
        return new RestfulResponse<>(0, "查询成功", info);
    }


    /**
     * 扫码出库确认
     */
    @PostMapping("/confirmOutboundScan")
    public RestfulResponse<OutboundShipQrCodeResultDTO> updateScanToOutboundShipConfirm(@RequestBody OutboundShipParamDTO dto) {
        shipHeaderService.updateOutShipConfirmScan(dto);
        return new RestfulResponse<>(0, "操作成功");
    }

    /**
     * 获取备料退库信息
     */
    @PostMapping("/getOutboundShipInfoForQuit")
    public RestfulResponse<OutboundShipQuitResultDTO> getOutboundShipInfoByVin(@RequestBody OutboundShipParamDTO dto) {
        OutboundShipQuitResultDTO infoForQuit = shipHeaderService.getOutboundShipInfoForQuit(dto);
        return new RestfulResponse<>(0, "查询成功", infoForQuit);
    }

    /**
     * 确认备料退库(需要确认 )
     */
    @PostMapping("/updateOutboundShipQuit")
    public RestfulResponse<Object> updateOutboundShipQuit(@RequestBody OutboundShipParamDTO dto) {
        shipHeaderService.updateOutboundShipQuit(dto);
        return new RestfulResponse<>(0, "操作成功");
    }

    /**
     * 查询出库记录
     */
    @PostMapping("/queryOutboundShip")
    public RestfulResponse<Page<OutboundShipDTO>> selectShipLinePage(@RequestBody Page<OutboundShipDTO> page) {
        RestfulResponse<Page<OutboundShipDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        Page<OutboundShipDTO> outPage = shipLineService.queryOutShipLinePage(page);
        restfulResponse.setData(outPage);
        return restfulResponse;
    }

    /**
     * 出库记录导出
     */
    @PostMapping("/exportOSData")
    public RestfulResponse<List<OutboundShipDTO>> exportOSData(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<OutboundShipDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        List<OutboundShipDTO> outPage = shipLineService.queryExportOSData(condition);
        restfulResponse.setData(outPage);
        return restfulResponse;
    }

    /**
     * BMS手动补 发运记录接口
     */
    @PostMapping("/wmsShipHandler")
    public RestfulResponse<Object> wmsShipHandler(@RequestBody Map<String, String> condition) {
        RestfulResponse<Object> restfulResponse = new RestfulResponse<>(0, "success");
        shipLineService.wmsShipHandler(condition);
        return restfulResponse;
    }

}
