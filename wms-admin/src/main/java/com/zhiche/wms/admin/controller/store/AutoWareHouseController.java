package com.zhiche.wms.admin.controller.store;

import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.service.stock.AutoInAndOutBoundService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;


/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:00 2018/12/14
 */
@RestController
@Api(value = "AutoWareHouse-API", description = "自动出入库")
@RequestMapping(value = "/autoWareHouse", produces = MediaType.APPLICATION_JSON_VALUE)
public class AutoWareHouseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AutoWareHouseController.class);

    @Autowired
    private AutoInAndOutBoundService autoInAndOutBoundService;

    /**
     * 目的仓库无人值守时自动入库
     *
     * @param condition {}
     * @return
     */
    @PostMapping(value = "/unattendedAutoInBound")
    @ApiOperation(value = "目的仓库无人值守时自动入库")
    public RestfulResponse unattendedAutoInBound (@RequestBody Map<String, String> condition) {
        LOGGER.info("/unattendedAutoInBound （目的仓库无人值守时自动入库） params : {} ", condition);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            if (Objects.isNull(condition)) {
                throw new BaseException(99, "参数不能为空");
            }
            autoInAndOutBoundService.unattendedAutoWareHouse(condition);
        } catch (BaseException be) {
            LOGGER.error("/unattendedAutoInBound （目的仓库无人值守时自动入库）  BaseException ERROR... ", be);
            result = new RestfulResponse(be.getCode(), be.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("/unattendedAutoInBound （目的仓库无人值守时自动入库）  ERROR... ", e);
            result = new RestfulResponse(99, "failed", null);
        }
        return result;
    }

    /**
     * 仓库无人值守时自动出库
     *
     * @param condition {}
     * @return
     */
    @PostMapping(value = "/unattendedAutoOutBound")
    @ApiOperation(value = "仓库无人值守时自动出库")
    public RestfulResponse unattendedAutoOutBound (@RequestBody Map<String, String> condition) {
        LOGGER.info("/unattendedAutoOutBound （仓库无人值守时自动出库） params : {} ", condition);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            if (Objects.isNull(condition)) {
                throw new BaseException(99, "参数不能为空");
            }
            autoInAndOutBoundService.unattendedAutoOutBound(condition);
        } catch (BaseException be) {
            LOGGER.error("/unattendedAutoOutBound （仓库无人值守时自动出库）  BaseException ERROR... ", be);
            result = new RestfulResponse(be.getCode(), be.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("/unattendedAutoOutBound （仓库无人值守时自动出库）  ERROR... ", e);
            result = new RestfulResponse(99, "failed", null);
        }
        return result;
    }
}
