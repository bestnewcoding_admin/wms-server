package com.zhiche.wms.admin.controller.inbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.inbound.InboundDTO;
import com.zhiche.wms.dto.inbound.InboundPutawayDTO;
import com.zhiche.wms.dto.inbound.LocChangeDTO;
import com.zhiche.wms.service.inbound.IInboundPutawayLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/inboundPutaway")
public class InboundPutawayController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundPutawayController.class);
    @Autowired
    private IInboundPutawayLineService iInboundPutawayLineService;

    @PostMapping("/selectPutawayPage")
    public RestfulResponse<Page<InboundPutawayDTO>> queryLinePage(@RequestBody Page<InboundPutawayDTO> page) {
        RestfulResponse<Page<InboundPutawayDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            Page<InboundPutawayDTO> outPage = iInboundPutawayLineService.queryLinePage(page);
            restfulResponse.setData(outPage);
        } catch (BaseException e) {
            LOGGER.error("Controller:" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("查询异常，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/exportINRData")
    public RestfulResponse<List<InboundPutawayDTO>> exportINRData(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<InboundPutawayDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        List<InboundPutawayDTO> data = iInboundPutawayLineService.queryExportData(condition);
        restfulResponse.setData(data);
        return restfulResponse;
    }


    @PostMapping("/getReceiveKeyInfo")
    public RestfulResponse<InboundPutawayDTO> getReceiveKeyInfo(@RequestBody Map<String, String> condition) {
        InboundPutawayDTO data = iInboundPutawayLineService.getReceiveKeyInfo(condition);
        return new RestfulResponse<>(0, "success", data);
    }

    @PostMapping("/getKeyPrintInfo")
    public RestfulResponse<InboundPutawayDTO> getKeyPrintInfo(@RequestBody Map<String, String> condition) {
        InboundPutawayDTO data = iInboundPutawayLineService.getKeyPrintInfo(condition);
        return new RestfulResponse<>(0, "success", data);
    }

    @PostMapping("/confirmReceiveKey")
    public RestfulResponse<String> confirmReceiveKey(@RequestBody Map<String, String> condition) {
        try {
            iInboundPutawayLineService.updateReceiveKey(condition);
        } catch (BaseException e) {
            //对自定义异常信息的处理
            return new RestfulResponse<>(e.getCode(), e.getMessage(), null);
        } catch (Exception e) {
            //对controller中的非自定义异常做处理
            return new RestfulResponse<>(-1, e.getMessage(), null);
        }
//        iInboundPutawayLineService.updateReceiveKey(condition);
        return new RestfulResponse<>(0, "success", null);
    }

    @PostMapping("/changeLocList")
    public RestfulResponse<Page<LocChangeDTO>> changeLocList(@RequestBody Page<LocChangeDTO> page) {
        Page<LocChangeDTO> data = iInboundPutawayLineService.queryChangLocList(page);
        return new RestfulResponse<>(0, "查询成功", data);
    }

    @PostMapping("/exportLocList")
    public RestfulResponse<List<LocChangeDTO>> exportLocList(@RequestBody Map<String,Object> condition) {
        List<LocChangeDTO> data = iInboundPutawayLineService.queryExportLocList(condition);
        return new RestfulResponse<>(0, "查询成功", data);
    }

    @PostMapping("/queryKeyStatus")
    public RestfulResponse queryKeyStatus(@RequestBody Map<String,String> condition) {
        RestfulResponse<Boolean> result = new RestfulResponse<>();
        result.setCode(0);
        result.setMessage("success");
        try{
            boolean flag = iInboundPutawayLineService.queryKeyStatus(condition);
            result.setData(flag);
        }catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
            result.setData(false);
        }

        return result;
    }

}
