package com.zhiche.wms.admin.controller.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.outbound.OutboundPreparationParamDTO;
import com.zhiche.wms.dto.outbound.PreparationListHeadDTO;
import com.zhiche.wms.dto.outbound.PreparationListDetailDTO;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 出库作业表现层
 */
@RestController
@RequestMapping("/preparationManage")
public class OutboundPreparationController {

    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;

    /**
     * 列表查询 备料计划
     */
    @PostMapping("/getPreparationList")
    public RestfulResponse<Page<PreparationListHeadDTO>> getPreparationList(
            @RequestBody Page<PreparationListHeadDTO> page) {
        Page<PreparationListHeadDTO> list = prepareHeaderService.queryOutPreparePage(page);
        return new RestfulResponse<>(0, "查询成功", list);
    }

    /**
     * 创建备料计划
     */
    @PostMapping("/createPreparation")
    public RestfulResponse<Object> createPreparation(@RequestBody OutboundPreparationParamDTO dto) {
        prepareHeaderService.createPreparation(dto);
        return new RestfulResponse<>(0, "操作成功");
    }

    /**
     * 获取备料计划明细信息
     */
    @PostMapping("/getPreparationDetail")
    public RestfulResponse<List<PreparationListDetailDTO>> getPreparationDetail(@RequestBody OutboundPreparationParamDTO dto) {
        List<PreparationListDetailDTO> prepareInfo = prepareHeaderService.getPreparationDetail(dto);
        return new RestfulResponse<>(0, "查询成功", prepareInfo);
    }

    /**
     * 备料任务变更
     */
    @PostMapping("/changePreparation")
    public RestfulResponse<Object> changePreparation(@RequestBody OutboundPreparationParamDTO dto) {
        prepareHeaderService.updateChangePreparation(dto);
        return new RestfulResponse<>(0, "操作成功");
    }

    /**
     * 单台备料
     */
    @PostMapping("/singlePreparation")
    public RestfulResponse<Object> singlePreparation(@RequestBody OutboundPreparationParamDTO dto) {
        prepareHeaderService.singlePreparation(dto);
        return new RestfulResponse<>(0, "操作成功");
    }

    /**
     * 列表查询 未备料计划
     */
    @PostMapping("/notPreparationList")
    public RestfulResponse<Page<PreparationListHeadDTO>> notPreparationList(
            @RequestBody Page<PreparationListHeadDTO> page) {
        Page<PreparationListHeadDTO> preparationListHeadDTOPage = prepareHeaderService.notPreparationList(page);
        return new RestfulResponse<>(0, "查询成功", preparationListHeadDTOPage);
    }
}
