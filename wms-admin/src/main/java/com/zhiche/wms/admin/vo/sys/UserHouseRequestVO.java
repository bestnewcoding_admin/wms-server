package com.zhiche.wms.admin.vo.sys;

import java.util.List;

/**
 * Created by hxh on 2018/9/20.
 */
public class UserHouseRequestVO {

    private Integer userId;
    private List<Long> houseIds;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<Long> getHouseIds() {
        return houseIds;
    }

    public void setHouseIds(List<Long> houseIds) {
        this.houseIds = houseIds;
    }
}
