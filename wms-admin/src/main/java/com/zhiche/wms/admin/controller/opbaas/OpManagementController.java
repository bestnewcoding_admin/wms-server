package com.zhiche.wms.admin.controller.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceHandlingParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.OrderReleaseParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskResultDTO;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import com.zhiche.wms.service.opbaas.IOrderReleaseService;
import com.zhiche.wms.service.opbaas.ITaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 提车管理 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2018-05-24
 */
@RestController
@RequestMapping("/OpManagement")
@Api(value = "/OpManagement", description = "提车管理接口")
public class OpManagementController {

    @Autowired
    private IOrderReleaseService iOrderReleaseService;

    @Autowired
    private ITaskService iTaskService;

    @Autowired
    private IExceptionRegisterService iExceptionRegisterService;

    /**
     * <p>
     * 提车管理-指令查询
     * </p>
     */
    @ApiOperation(value = "指令列表", notes = "指令列表返回ResultDTO的json对象")
    @PostMapping("/getShipmentList")
    public RestfulResponse<Page<OrderReleaseParamDTO>> getShipmentList(@RequestBody Page<OrderReleaseParamDTO> page) {
        RestfulResponse<Page<OrderReleaseParamDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        Page<OrderReleaseParamDTO> opPage = iOrderReleaseService.queryOrderReleaseList(page);
        restfulResponse.setData(opPage);
        return restfulResponse;
    }

    /**
     * <p>
     * 提车管理-任务查询
     * </p>
     */
    @ApiOperation(value = "任务列表", notes = "任务列表返回ResultDTO的json对象")
    @PostMapping("/getTaskList")
    public RestfulResponse<Page<TaskResultDTO>> getTaskList(@RequestBody Page<TaskResultDTO> page) {
        RestfulResponse<Page<TaskResultDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        Page<TaskResultDTO> opPage = iTaskService.queryTaskList(page);
        restfulResponse.setData(opPage);
        return restfulResponse;
    }

    /**
     * <p>
     * 异常列表查询
     * </p>
     */
    @ApiOperation(value = "异常列表", notes = "异常列表返回ResultDTO的json对象")
    @PostMapping("/getExceptionList")
    public RestfulResponse<Page<ExResultDTO>> getExceptionList(@RequestBody Page<ExResultDTO> page) {
        RestfulResponse<Page<ExResultDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        Page<ExResultDTO> opPage = iExceptionRegisterService.queryExceptionList(page);
        restfulResponse.setData(opPage);
        return restfulResponse;
    }

    /**
     * <p>
     * 异常列表导出
     * </p>
     */
    @ApiOperation(value = "异常列表导出", notes = "异常列表返回ResultDTO的json对象")
    @PostMapping("/exportExcpList")
    public RestfulResponse<List<ExResultDTO>> exportExcpList(@RequestBody Page<ExResultDTO> page) {
        RestfulResponse<List<ExResultDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        List<ExResultDTO> exportExcpList = iExceptionRegisterService.queryExportExcpList(page);
        restfulResponse.setData(exportExcpList);
        return restfulResponse;
    }

    /**
     * <p>
     * 异常明细
     * </p>
     */
    @ApiOperation(value = "异常明细", notes = "异常列表返回ResultDTO的json对象")
    @PostMapping("/getExceptionDetail")
    public RestfulResponse<Page<ExResultDTO>> getExceptionDetail(@RequestBody Page<ExResultDTO> page) {
        RestfulResponse<Page<ExResultDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        Page<ExResultDTO> opPage = iExceptionRegisterService.queryExceptionDetail(page);
        restfulResponse.setData(opPage);
        return restfulResponse;
    }

    /**
     * <p>
     * 异常处理方式
     * </p>
     */
    @ApiOperation(value = "异常处理方式")
    @PostMapping("/exceptionHandling")
    public RestfulResponse<String> getExceptionDetail(@RequestBody ExceHandlingParamDTO exceHandlingParamDTO) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success");
        iExceptionRegisterService.exceptionHandling(exceHandlingParamDTO);
        return restfulResponse;
    }
}
