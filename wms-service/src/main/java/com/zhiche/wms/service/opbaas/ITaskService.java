package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.OpTask;
import com.zhiche.wms.dto.base.ResultDTOWithPagination;
import com.zhiche.wms.dto.opbaas.paramdto.TaskControllerParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskDetailResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskResultDTO;

import java.util.List;

/**
 * <p>
 * 作业任务 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface ITaskService extends IService<OpTask> {

    List<TaskResultDTO> queryTaskInfo(TaskControllerParamDTO dto);

    ResultDTOWithPagination<List<TaskResultDTO>> queryTaskList(TaskControllerParamDTO dto);

    TaskDetailResultDTO getDetailByScan(TaskControllerParamDTO dto);

    TaskDetailResultDTO getTaskDetail(TaskControllerParamDTO dto);

    void updatePickTask(TaskControllerParamDTO dto);

    void updateMoveTask(TaskControllerParamDTO dto);

    ResultDTOWithPagination<List<TaskResultDTO>> getMyTaskByType(TaskControllerParamDTO dto);

    /**
     * 提车管理-任务查询
     */
    Page<TaskResultDTO> queryTaskList(Page<TaskResultDTO> page);

    /**
     * 使用page封装查询任务列表
     */
    Page<TaskResultDTO> queryTaskPage(TaskControllerParamDTO dto);
}
