package com.zhiche.wms.service.opbaas;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.MissingComponentRegister;
import com.zhiche.wms.dto.opbaas.paramdto.MissingRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExConfigurationDTO;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;

import java.util.List;

/**
 * <p>
 * 缺件登记 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-07
 */
public interface IMissingComponentRegisterService extends IService<MissingComponentRegister> {

    /**
     * 获取缺件信息
     */
    List<MissingRegidterDetailDTO> queryMissingInfo(MissingRegisterParamDTO dto);

    /**
     * 编辑缺件信息
     */
    List<MissingRegidterDetailDTO> updateMissingInfo(MissingRegisterParamDTO dto);
}
