package com.zhiche.wms.service.movement;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.movement.MovementLine;

/**
 * <p>
 * 移位明细 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IMovementLineService extends IService<MovementLine> {

}
