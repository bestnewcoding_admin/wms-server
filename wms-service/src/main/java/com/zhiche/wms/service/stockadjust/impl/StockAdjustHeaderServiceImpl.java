package com.zhiche.wms.service.stockadjust.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.stockadjust.StockAdjustHeaderMapper;
import com.zhiche.wms.domain.model.stock.StockProperty;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustHeader;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustLine;
import com.zhiche.wms.service.stock.impl.StockServiceImpl;
import com.zhiche.wms.service.stockadjust.IStockAdjustHeaderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 库存调整单头 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@Service
public class StockAdjustHeaderServiceImpl extends ServiceImpl<StockAdjustHeaderMapper, StockAdjustHeader> implements IStockAdjustHeaderService {

    @Autowired
    private SnowFlakeId snowFlakeId;

    @Autowired
    private StockAdjustLineServiceImpl stockAdjustLineService;

    @Autowired
    private StockServiceImpl stockService;

    @Override
    @Transactional
    public boolean creatStockAdjust(StockAdjustHeader stockAdjustHeader) {
        stockAdjustHeader.setId(snowFlakeId.nextId());
        for (StockAdjustLine ml : stockAdjustHeader.getStockAdjustLineList()) {
            ml.setId(snowFlakeId.nextId());
            ml.setHeaderId(stockAdjustHeader.getId());
            stockAdjustLineService.insert(ml);
        }
        stockAdjustHeader.setLineCount(stockAdjustHeader.getStockAdjustLineList().size());
        stockAdjustHeader.setStatus("10");
        return insert(stockAdjustHeader);
    }

    @Override
    @Transactional
    public boolean auditStockAdjust(Long stockAdjustId) {
        StockAdjustHeader stockAdjustHeader = selectById(stockAdjustId);
        if (Objects.isNull(stockAdjustHeader)) throw new BaseException(4010, "期初库存不存在！");

        EntityWrapper<StockAdjustLine> ew = new EntityWrapper<>();
        ew.eq("header_id", stockAdjustHeader.getId());
        List<StockAdjustLine> stockAdjustLineList = stockAdjustLineService.selectList(ew);
        if (Objects.isNull(stockAdjustLineList)) throw new BaseException(4020, "期初库存明细不存在！");

        for (StockAdjustLine ml : stockAdjustLineList) {
            updateStockQty(stockAdjustHeader.getStoreHouseId(), ml);
        }
        stockAdjustHeader.setStatus("20");
        stockAdjustHeader.setGmtCreate(null);//创建时间使用数据库自行处理
        stockAdjustHeader.setGmtModified(null);//更新时间使用数据库自行处理
        return updateById(stockAdjustHeader);
    }

    @Override
    @Transactional
    public boolean creatAndAuditStockAdjust(StockAdjustHeader stockAdjustHeader) {
        stockAdjustHeader.setId(snowFlakeId.nextId());
        for (StockAdjustLine ml : stockAdjustHeader.getStockAdjustLineList()) {
            ml.setId(snowFlakeId.nextId());
            ml.setHeaderId(stockAdjustHeader.getId());

            if (stockAdjustLineService.insert(ml)) {
                updateStockQty(stockAdjustHeader.getStoreHouseId(), ml);
            } else {
                throw new BaseException("保存移位单明细失败！");
            }
        }

        stockAdjustHeader.setLineCount(stockAdjustHeader.getStockAdjustLineList().size());
        stockAdjustHeader.setStatus("20");
        return insert(stockAdjustHeader);
    }

    @Override
    @Transactional
    public boolean cancelAuditStockAdjust(Long stockAdjustId) {
        StockAdjustHeader stockAdjustHeader = selectById(stockAdjustId);
        if (Objects.isNull(stockAdjustHeader)) throw new BaseException(4010, "期初库存不存在！");

        EntityWrapper<StockAdjustLine> ew = new EntityWrapper<>();
        ew.eq("header_id", stockAdjustHeader.getId());
        List<StockAdjustLine> stockAdjustLineList = stockAdjustLineService.selectList(ew);
        if (Objects.isNull(stockAdjustLineList)) throw new BaseException(4020, "期初库存明细不存在！");

        for (StockAdjustLine ml : stockAdjustLineList) {
            cancelStockQty(stockAdjustHeader.getStoreHouseId(), ml);
        }
        stockAdjustHeader.setStatus("30");
        stockAdjustHeader.setGmtCreate(null);//创建时间使用数据库自行处理
        stockAdjustHeader.setGmtModified(null);//更新时间使用数据库自行处理
        return updateById(stockAdjustHeader);
    }


    /**
     * 更新库存量
     *
     * @param storeHouseId 仓库ID
     * @param ml
     * @return
     */
    private boolean updateStockQty(Long storeHouseId, StockAdjustLine ml) {
        StockProperty stockProperty = new StockProperty();
        BeanUtils.copyProperties(ml, stockProperty);
        stockProperty.setStoreHouseId(storeHouseId);
        stockService.addStock(stockProperty, "30", ml.getId());
        return true;
    }

    /**
     * 撤销库存量
     *
     * @param storeHouseId 仓库ID
     * @param ml
     * @return
     */
    private boolean cancelStockQty(Long storeHouseId, StockAdjustLine ml) {
        StockProperty stockProperty = new StockProperty();
        BeanUtils.copyProperties(ml, stockProperty);
        stockProperty.setStoreHouseId(storeHouseId);
        stockProperty.setLocationId(ml.getLocationId());//减少目标储位的库存
        stockService.minusStock(stockProperty, "35", ml.getId());
        return true;
    }
}
