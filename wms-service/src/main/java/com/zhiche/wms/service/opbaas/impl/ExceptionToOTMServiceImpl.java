package com.zhiche.wms.service.opbaas.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.wms.configuration.MyConfigurationProperties;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.InterfaceAddrEnum;
import com.zhiche.wms.core.supports.enums.NodeOptionEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.domain.model.base.StorehouseNode;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionIsCanSendDTO;
import com.zhiche.wms.service.opbaas.ExceptionToOTMService;
import com.zhiche.wms.service.opbaas.IDeliveryPointService;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import com.zhiche.wms.service.sys.IStorehouseNodeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 异常推送otm
 * </p>
 *
 * @author yzy
 * @since 2018-11-09
 */
@Service
public class ExceptionToOTMServiceImpl implements ExceptionToOTMService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionToOTMServiceImpl.class);

    @Autowired
    private IStorehouseNodeService storehouseNodeService;

    @Autowired
    private IExceptionRegisterService registerService;

    @Autowired
    private MyConfigurationProperties properties;

    @Autowired
    private IExceptionRegisterService exceptionRegisterService;

    @Autowired
    private IDeliveryPointService deliveryPointService;

    public void isCanSend(CommonConditionParamDTO conditionParamDTO){
        if (conditionParamDTO == null) {
            throw new BaseException("参数不能为空");
        }
        Map<String, String> condition = conditionParamDTO.getCondition();
        if (condition == null || condition.isEmpty()) {
            throw new BaseException("condition不能为空");
        }
        if (StringUtils.isBlank(condition.get("houseId"))) {
            throw new BaseException("仓库id不能为空");
        }
        if (StringUtils.isBlank(condition.get("lotNo1"))) {
            throw new BaseException("车架号不能为空");
        }
        if (StringUtils.isNotBlank(condition.get("isCanSend"))) {

            ExceptionRegister exceptionRegister = new ExceptionRegister();
            exceptionRegister.setHouseId(condition.get("houseId"));

            ExceptionIsCanSendDTO exceptionIsCanSendDTO = new ExceptionIsCanSendDTO();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            exceptionIsCanSendDTO.setRegister_time(simpleDateFormat.format(new Date()));
            exceptionIsCanSendDTO.setVin_code(condition.get("lotNo1"));
            String result = null;
            if (TableStatusEnum.STATUS_N.getCode().equals(condition.get("isCanSend"))) {
                exceptionIsCanSendDTO.setIs_send(TableStatusEnum.STATUS_N.getCode());
                exceptionRegister.setOtmStatus(TableStatusEnum.STATUS_N.getCode());
                result = HttpClientUtil.postJson(properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress(), null, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getSocketTimeOut());
                LOGGER.info("异常处理推送otm result:{},params:{},url:{}", result, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress());
            } else if (TableStatusEnum.STATUS_Y.getCode().equals(condition.get("isCanSend"))) {
                exceptionIsCanSendDTO.setIs_send(TableStatusEnum.STATUS_Y.getCode());
                exceptionRegister.setOtmStatus(TableStatusEnum.STATUS_Y.getCode());
                result = HttpClientUtil.postJson(properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress(), null, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getSocketTimeOut());
                System.err.println(JSONObject.toJSONString(exceptionIsCanSendDTO));
                LOGGER.info("异常处理推送otm result:{},params:{},url:{}", result, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress());
            }
            JSONObject jsonObject = JSONObject.parseObject(result);
            if (jsonObject.getString("messageType").equals("success")) {
                Wrapper<ExceptionRegister> exceptionRegisterWrapper = new EntityWrapper<>();
                exceptionRegisterWrapper.eq("vin", condition.get("lotNo1"));
                registerService.update(exceptionRegister, exceptionRegisterWrapper);
            }
        }
    }

    public String isSend(String vin ,String originName) {
        Wrapper<ExceptionRegister> exceptionRegisterWrapper = new EntityWrapper<>();
        exceptionRegisterWrapper.eq("vin", vin);
        ExceptionRegister exceptionRegister = exceptionRegisterService.selectOne(exceptionRegisterWrapper);
        if (Objects.isNull(exceptionRegister)||StringUtils.isEmpty(exceptionRegister.getOtmStatus())) {
            Wrapper<OpDeliveryPoint> opDeliveryPointWrapper=new EntityWrapper<>();
            opDeliveryPointWrapper.eq("name",originName);
            OpDeliveryPoint opDeliveryPoint = deliveryPointService.selectOne(opDeliveryPointWrapper);
            if (!Objects.isNull(opDeliveryPoint)){
                if (!StringUtils.isEmpty(opDeliveryPoint.getExceShip())&&opDeliveryPoint.getExceShip().equals(TableStatusEnum.STATUS_N.getCode())) {
                    return  TableStatusEnum.STATUS_N.getCode();
                }
                if (!StringUtils.isEmpty(opDeliveryPoint.getExceShip())&&opDeliveryPoint.getExceShip().equals(TableStatusEnum.STATUS_Y.getCode())) {
                    return TableStatusEnum.STATUS_Y.getCode();
                }
            }
        } else {
            return exceptionRegister.getOtmStatus();
        }
        return null;
    }

}
