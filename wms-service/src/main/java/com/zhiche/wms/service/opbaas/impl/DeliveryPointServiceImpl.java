package com.zhiche.wms.service.opbaas.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.opbaas.DeliveryPointMapper;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.dto.opbaas.resultdto.OpDeliveryPointResultDTO;
import com.zhiche.wms.service.opbaas.IDeliveryPointService;
import com.zhiche.wms.service.sys.IUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 发车点配置 服务实现类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Service
public class DeliveryPointServiceImpl extends ServiceImpl<DeliveryPointMapper, OpDeliveryPoint> implements IDeliveryPointService {
    @Autowired
    private SnowFlakeId flakeId;
    @Autowired
    private IUserService userService;


    /**
     * 查询用户下发车点
     */
    @Override
    public Page<OpDeliveryPointResultDTO> listPointPage(Page<OpDeliveryPointResultDTO> page) {
        if (page == null) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> cn = page.getCondition();
        if (cn == null || cn.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        EntityWrapper<OpDeliveryPoint> deEW = new EntityWrapper<>();
        if (cn.get("status") != null && StringUtils.isNotBlank(cn.get("status").toString())) {
            deEW.eq("status", cn.get("status").toString());
        }
        if (cn.get("name") != null && StringUtils.isNotBlank(cn.get("name").toString())) {
            deEW.like("name", cn.get("name").toString());
        }
        deEW.orderBy("id", false);
        List<OpDeliveryPoint> list = baseMapper.selectPage(page, deEW);
        ArrayList<OpDeliveryPointResultDTO> records = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(list)) {
            for (OpDeliveryPoint op : list) {
                OpDeliveryPointResultDTO dto = new OpDeliveryPointResultDTO();
                BeanUtils.copyProperties(op, dto);
                records.add(dto);
            }
        }
        page.setRecords(records);
        return page;
    }

    /**
     * 保存发车点配置
     */
    @Override
    public void savePoint(OpDeliveryPointResultDTO dto) {
        User loginUser = checkCommonDTO(dto);
        //重复校验
        EntityWrapper<OpDeliveryPoint> odpEW = new EntityWrapper<>();
        odpEW.eq("code", dto.getCode());
        int count = selectCount(odpEW);
        if (count > 0) {
            throw new BaseException("该发车点code:" + dto.getCode() + "已经存在");
        }
        OpDeliveryPoint point = new OpDeliveryPoint();
        BeanUtils.copyProperties(dto, point);
        point.setId(String.valueOf(flakeId.nextId()));
        String[] option = dto.getNodeOption();
        setPoint(option, point);
        point.setUserCreate(loginUser.getName());
        point.setUserModified(loginUser.getName());
        insert(point);
    }

    /**
     * 修改发车点
     */
    @Override
    public void updatePoint(OpDeliveryPointResultDTO dto) {
        User user = checkCommonDTO(dto);
        if (StringUtils.isBlank(dto.getId())) {
            throw new BaseException("id不能为空");
        }
        String[] option = dto.getNodeOption();
        OpDeliveryPoint point = new OpDeliveryPoint();
        BeanUtils.copyProperties(dto, point);
        setPoint(option, point);
        point.setUserModified(user.getName());
        point.setGmtModified(null);
        updateById(point);
    }

    @Override
    public Page<OpDeliveryPoint> allUsablePoint(Page<OpDeliveryPoint> page) {
        Wrapper<OpDeliveryPoint> ew = new EntityWrapper<>();
        Map<String, Object> condition = page.getCondition();
        if (condition!=null){
            if (StringUtils.isNotBlank(String.valueOf(condition.get("name")))){
                ew.like("name",condition.get("name").toString());
            }
        }
        ew.eq("status",TableStatusEnum.STATUS_10.getCode());
        List<OpDeliveryPoint> opDeliveryPoints = baseMapper.selectPage(page, ew);
        page.setRecords(opDeliveryPoints);
        return page;
    }

    private void setPoint(String[] option, OpDeliveryPoint point) {
        point.setIsSeek(TableStatusEnum.STATUS_0.getCode());
        point.setIsMove(TableStatusEnum.STATUS_0.getCode());
        point.setIsPick(TableStatusEnum.STATUS_0.getCode());
        point.setIsShip(TableStatusEnum.STATUS_0.getCode());
        for (String op : option) {
            if (TableStatusEnum.STATUS_IS_SEEK.getCode().equals(op)) {
                point.setIsSeek(TableStatusEnum.STATUS_1.getCode());
            } else if (TableStatusEnum.STATUS_IS_MOVE.getCode().equals(op)) {
                point.setIsMove(TableStatusEnum.STATUS_1.getCode());
            } else if (TableStatusEnum.STATUS_IS_PICK.getCode().equals(op)) {
                point.setIsPick(TableStatusEnum.STATUS_1.getCode());
            } else if (TableStatusEnum.STATUS_IS_SHIP.getCode().equals(op)) {
                point.setIsShip(TableStatusEnum.STATUS_1.getCode());
            } else {
                throw new BaseException("不能识别的关联节点:" + op);
            }
        }
    }

    private User checkCommonDTO(OpDeliveryPointResultDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空");
        }
        if (StringUtils.isBlank(dto.getCode())) {
            throw new BaseException("发车点编码不能为空");
        }
        if (StringUtils.isBlank(dto.getName())) {
            throw new BaseException("发车点名称不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        return loginUser;
    }

    @Override
    public List<OpDeliveryPoint> userPoint(Integer userId) {

        return  baseMapper.userPoint(userId);
    }
}
