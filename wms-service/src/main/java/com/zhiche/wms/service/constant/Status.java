package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/12.
 */
public class Status {
    public static class Inbound{
        /**
         * 未入库
         */
        public static final String NOT="10";

        /**
         * 部分入库
         */
        public static final String PART="20";

        /**
         * 全部入库
         */
        public static final String ALL="30";

        /**
         * 关闭入库
         */
        public static final String CLOSE="40";

        /**
         * 取消
         */
        public static final String CANCEL="50";
    }

    public static class Outbound{
        /**
         * 未出库
         */
        public static final String NOT="10";

        /**
         * 部分出库
         */
        public static final String PART="20";

        /**
         * 全部出库
         */
        public static final String ALL="30";

        /**
         * 关闭出库
         */
        public static final String CLOSE="40";

        /**
         * 取消
         */
        public static final String CANCEL="50";
    }


    /**
     * 保存
     */
    public static final String SAVE = "10";

    /**
     * 审核
     */
    public static final String AUDIT = "20";

    /**
     * 取消
     */
    public static final String CANCEL = "50";

}
