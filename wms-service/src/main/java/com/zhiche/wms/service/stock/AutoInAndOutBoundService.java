package com.zhiche.wms.service.stock;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stock.Stock;

import java.util.Map;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 15:36 2018/12/14
 */
public interface AutoInAndOutBoundService extends IService<Stock> {

    /**
     * 无人值守自动入库
     *
     * @param condition
     */
    void unattendedAutoWareHouse (Map<String, String> condition);


    /**
     * 仓库无人值守自动出库
     *
     * @param condition 出库条件
     */
    void unattendedAutoOutBound (Map<String, String> condition);
}
