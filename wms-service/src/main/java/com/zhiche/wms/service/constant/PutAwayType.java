package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/11.
 */
public class PutAwayType {

    /**
     * 通知入库
     */
    public static final String NOTICE_PUTAWAY = "10"; //通知入库

    /**
     * 盲收入库
     */
    public static final String BLIND_PUTAWAY = "20"; //盲收入库

    /**
     * 维修入库
     */
    public static final String REPAIR_PUTAWAY = "30"; //维修入库

    /**
     * 还车入库
     */
    public static final String REVERT_PUTAWAY = "40"; //还车入库

    /**
     * 备料退库
     */
    public static final String BACK_PUTAWAY = "50"; //备料退库
}
