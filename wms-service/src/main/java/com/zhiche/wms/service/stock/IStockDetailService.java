package com.zhiche.wms.service.stock;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stock.StockDetail;

/**
 * <p>
 * 库存明细账 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IStockDetailService extends IService<StockDetail> {

}
