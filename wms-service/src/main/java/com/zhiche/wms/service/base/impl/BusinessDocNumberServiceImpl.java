package com.zhiche.wms.service.base.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.domain.mapper.base.BusinessDocNumberMapper;
import com.zhiche.wms.domain.model.base.BusinessDocNumber;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import com.zhiche.wms.service.constant.DocPrefix;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>
 * 业务单据号 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-06-07
 */
@Service
public class BusinessDocNumberServiceImpl extends ServiceImpl<BusinessDocNumberMapper, BusinessDocNumber> implements IBusinessDocNumberService {


    @Override
    public String updateNextByProcedure(String prefix) {
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("prefix", prefix + getDateStr());
        params.put("nextValOut","");
        baseMapper.getNextByProcedure(params);
        if (params.get("nextValOut") == null || StringUtils.isBlank(params.get("nextValOut").toString())) {
            throw new BaseException("获取单据号异常");
        }
        return params.get("nextValOut").toString();
    }

    @Override
    public  String getInboundNoticeNo() {
        return updateNextByProcedure(DocPrefix.INBOUND_NOTICE);
    }

    @Override
    public  String getInboundInspectNo() {
        return updateNextByProcedure(DocPrefix.INBOUND_INSPECT);
    }

    @Override
    public  String getInboundPutAwayNo() {
        return updateNextByProcedure(DocPrefix.INBOUND_PUTAWAY);
    }

    @Override
    public  String getOutboundNoticeNo() {
        return updateNextByProcedure(DocPrefix.OUTBOUND_NOTICE);
    }

    @Override
    public  String getOutboundPrepareNo() {
        return updateNextByProcedure(DocPrefix.OUTBOUND_PREPARE);
    }

    @Override
    public  String getOutboundShipNo() {
        return updateNextByProcedure(DocPrefix.OUTBOUND_SHIP);
    }

    @Override
    public  String getMovementNo() {
        return updateNextByProcedure(DocPrefix.MOVEMENT);
    }

    @Override
    public  String getStockInitNo() {
        return updateNextByProcedure(DocPrefix.STOCK_INIT);
    }

    @Override
    public  String getStockAdjustNo() {
        return updateNextByProcedure(DocPrefix.STOCK_ADJUST);
    }

    @Override
    public int getNextValue(String prefix) {
        int nextValue;
        Wrapper<BusinessDocNumber> ew = new EntityWrapper<>();
        ew.eq("prefix", prefix);
        BusinessDocNumber businessDocNumber = selectOne(ew);

        if (Objects.isNull(businessDocNumber)) {
            nextValue = 1;
            businessDocNumber = new BusinessDocNumber();
            businessDocNumber.setPrefix(prefix);
            businessDocNumber.setCurrentValue(nextValue);
            businessDocNumber.setNextValue(nextValue + 1);
            insert(businessDocNumber);
        } else {
            nextValue = businessDocNumber.getNextValue();
            businessDocNumber.setCurrentValue(nextValue);
            businessDocNumber.setNextValue(nextValue + 1);
            updateById(businessDocNumber);
        }
        return nextValue;
    }

//    @Override
//    public synchronized String getInboundNoticeNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.INBOUND_NOTICE +
//                dateStr + getFourCode(getNextValue(DocPrefix.INBOUND_NOTICE + dateStr));
//    }
//
//    @Override
//    public synchronized String getInboundInspectNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.INBOUND_INSPECT +
//                dateStr + getFourCode(getNextValue(DocPrefix.INBOUND_INSPECT + dateStr));
//    }
//
//    @Override
//    public synchronized String getInboundPutAwayNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.INBOUND_PUTAWAY +
//                dateStr + getFourCode(getNextValue(DocPrefix.INBOUND_PUTAWAY + dateStr));
//    }
//
//    @Override
//    public synchronized String getOutboundNoticeNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.OUTBOUND_NOTICE +
//                dateStr + getFourCode(getNextValue(DocPrefix.OUTBOUND_NOTICE + dateStr));
//    }
//
//    @Override
//    public synchronized String getOutboundPrepareNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.OUTBOUND_PREPARE +
//                dateStr + getFourCode(getNextValue(DocPrefix.OUTBOUND_PREPARE + dateStr));
//    }
//
//    @Override
//    public synchronized String getOutboundShipNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.OUTBOUND_SHIP +
//                dateStr + getFourCode(getNextValue(DocPrefix.OUTBOUND_SHIP + dateStr));
//    }
//
//    @Override
//    public synchronized String getMovementNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.MOVEMENT +
//                dateStr + getFourCode(getNextValue(DocPrefix.MOVEMENT + dateStr));
//    }
//
//    @Override
//    public synchronized String getStockInitNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.STOCK_INIT +
//                dateStr + getFourCode(getNextValue(DocPrefix.STOCK_INIT + dateStr));
//    }
//
//    @Override
//    public synchronized String getStockAdjustNo() {
//        String dateStr = getDateStr();
//        return DocPrefix.STOCK_ADJUST +
//                dateStr + getFourCode(getNextValue(DocPrefix.STOCK_ADJUST + dateStr));
//    }

    private  String getDateStr() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        return formatter.format(currentTime);
    }

    private  String getFourCode(int value) {
        return String.format("%04d", value);
    }
}
