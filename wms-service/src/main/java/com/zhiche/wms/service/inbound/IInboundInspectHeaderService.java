package com.zhiche.wms.service.inbound;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.inbound.InboundInspectHeader;

import java.math.BigDecimal;

/**
 * <p>
 * 入库质检单头 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
public interface IInboundInspectHeaderService extends IService<InboundInspectHeader> {
    Integer updateStatus(Long id,Long houseId,Integer status, BigDecimal qualifiedSum,BigDecimal damagedSum);
}
