package com.zhiche.wms.service.surpports;

import java.util.HashMap;
import java.util.Map;

public interface IHnInterfaceService {
    /**
     * 重构 霍尼扫码自动入库
     */
    HashMap<String, Object> updateInboundCar(Map<String, String> condition);

    /**
     * 重构 霍尼扫码自动出库
     */
    HashMap<String, Object> updateOutBoundCar(Map<String, String> condition);

}
