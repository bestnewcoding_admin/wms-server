package com.zhiche.wms.service.common.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.zhiche.wms.configuration.MyConfigurationProperties;
import com.zhiche.wms.core.supports.enums.InterfaceAddrEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.log.ItfExplogHeader;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.service.common.IntegrationService;
import com.zhiche.wms.service.dto.OTMEvent;
import com.zhiche.wms.service.dto.ShipParamDTO;
import com.zhiche.wms.service.log.IItfExplogHeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@Service
public class IntegrationServiceImpl implements IntegrationService {
    @Autowired
    private SnowFlakeId flakeId;
    @Autowired
    private MyConfigurationProperties properties;
    @Autowired
    private IItfExplogHeaderService explogHeaderService;

    @Override
    public OTMEvent getOtmEvent(String key,
                                String releaseGid,
                                String eventType,
                                String shipmentGid,
                                String describe) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        OTMEvent event = new OTMEvent();
        event.setExportKey(String.valueOf(key));
        event.setCallBackUrl(null);
        event.setSort(1);
        event.setEventType(eventType);
        event.setOccurDate(sdf.format(new Date()));
        event.setRecdDate(sdf.format(new Date()));
        event.setDescribe(describe);
        event.setOrderReleaseId(releaseGid);
        event.setShipmentId(shipmentGid);
        return event;
    }

    @Override
    public void insertExportLog(String key,
                                OTMEvent event,
                                String res,
                                String describe,
                                String exportType) {
        ItfExplogHeader exH = new ItfExplogHeader();
        String jsonString = JSONObject.toJSONString(event);
        exH.setId(flakeId.nextId());
        exH.setExportType(exportType);
        exH.setRelationId(Long.valueOf(key));
        exH.setTargetSource(TableStatusEnum.STATUS_10.getCode());
        exH.setExportStatus(TableStatusEnum.STATUS_1.getCode());
        exH.setExportRemarks(describe);
        exH.setRequestId(res);

        ItfExplogLine exl = new ItfExplogLine();
        exl.setId(flakeId.nextId());
        exl.setHeaderId(exH.getId());
        exl.setExportType(exportType);
        exl.setRelationId(Long.valueOf(key));
        exl.setTargetSource(TableStatusEnum.STATUS_10.getCode());
        exl.setInterfaceUrl(properties.getIntegrationhost() + InterfaceAddrEnum.EVENT_URI.getAddress());
        exl.setExportStartTime(new Date());
        exl.setExportStatus(TableStatusEnum.STATUS_1.getCode());
        exl.setExportRemarks(describe);
        exl.setDataContent(jsonString);
        exl.setRequestId(res);
        ArrayList<ItfExplogLine> lines = Lists.newArrayList();
        lines.add(exl);
        exH.setItfExplogLines(lines);
        explogHeaderService.insertWithLines(exH);
    }

    @Override
    public void insertExportLogNew(String key,
                                   ShipParamDTO paramDTO,
                                   String res, String describe,
                                   String exportType) {
        String jsonString = JSONObject.toJSONString(paramDTO);
        ItfExplogHeader exH = new ItfExplogHeader();
        exH.setId(flakeId.nextId());
        exH.setExportType(exportType);
        exH.setRelationId(Long.valueOf(key));
        exH.setTargetSource(TableStatusEnum.STATUS_10.getCode());
        exH.setExportStatus(TableStatusEnum.STATUS_1.getCode());
        exH.setExportRemarks(describe);
        exH.setRequestId(res);

        ItfExplogLine exl = new ItfExplogLine();
        exl.setId(flakeId.nextId());
        exl.setHeaderId(exH.getId());
        exl.setExportType(exportType);
        exl.setRelationId(Long.valueOf(key));
        exl.setTargetSource(TableStatusEnum.STATUS_10.getCode());
        exl.setInterfaceUrl(properties.getIntegrationhost() + InterfaceAddrEnum.EVENT_URI.getAddress());
        exl.setExportStartTime(new Date());
        exl.setExportStatus(TableStatusEnum.STATUS_1.getCode());
        exl.setExportRemarks(describe);
        exl.setDataContent(jsonString);
        exl.setRequestId(res);
        ArrayList<ItfExplogLine> lines = Lists.newArrayList();
        lines.add(exl);
        exH.setItfExplogLines(lines);
        explogHeaderService.insertWithLines(exH);
    }
}
