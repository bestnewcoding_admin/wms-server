package com.zhiche.wms.service.opbaas.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.google.common.collect.Maps;
import com.zhiche.wms.configuration.MyConfigurationProperties;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceAddrEnum;
import com.zhiche.wms.core.supports.enums.InterfaceEventEnum;
import com.zhiche.wms.core.supports.enums.InterfaceVisitTypeEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.mapper.otm.OtmShipmentMapper;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.domain.model.opbaas.StatusLog;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.domain.model.sys.UserDeliveryPoint;
import com.zhiche.wms.dto.opbaas.paramdto.AppCommonQueryDTO;
import com.zhiche.wms.dto.opbaas.paramdto.OrderReleaseParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.*;
import com.zhiche.wms.service.common.IntegrationService;
import com.zhiche.wms.service.constant.PutAwayType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.dto.OTMEvent;
import com.zhiche.wms.service.dto.ShipParamDTO;
import com.zhiche.wms.service.opbaas.*;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipLineService;
import com.zhiche.wms.service.sys.IUserDeliveryPointService;
import com.zhiche.wms.service.sys.IUserService;
import com.zhiche.wms.service.utils.BusinessNodeExport;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 * 运单 服务实现类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Service
public class OrderReleaseServiceImpl extends ServiceImpl<OtmOrderReleaseMapper, OtmOrderRelease> implements IOrderReleaseService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderReleaseServiceImpl.class);
    @Autowired
    private IUserService userService;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private IOtmShipmentService shipmentService;
    @Autowired
    private BusinessNodeExport nodeExport;
    @Autowired
    private IStatusLogService statusLogService;
    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;
    @Autowired
    private IOutboundShipLineService outboundShipLineService;
    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;
    @Autowired
    private IUserDeliveryPointService userDeliveryPointService;
    @Autowired
    private IDeliveryPointService deliveryPointService;
    @Autowired
    private ExceptionToOTMService exceptionToOTMService;
    @Autowired
    private IExceptionRegisterService exceptionRegisterService;
    @Autowired
    private MyConfigurationProperties properties;
    @Autowired
    private IOtmShipmentService otmShipmentService;
    @Autowired
    private IOtmOrderReleaseService otmOrderReleaseService;
    @Autowired
    private OtmShipmentMapper otmShipmentMapper;



    @Override
    public Page<OrderReleaseParamDTO> queryOrderReleaseList(Page<OrderReleaseParamDTO> page) {
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        Wrapper<UserDeliveryPoint> userDeliveryPointWrapper = new EntityWrapper<>();
        userDeliveryPointWrapper.eq("user_id", loginUser.getId());
        List<UserDeliveryPoint> userDeliveryPoints = userDeliveryPointService.selectList(userDeliveryPointWrapper);
        if (CollectionUtils.isEmpty(userDeliveryPoints)) {
            throw new BaseException("未配置发车点,不能进行此操作");
        }
        ArrayList<Object> PointIds = new ArrayList<>();
        for (UserDeliveryPoint userDeliveryPoint : userDeliveryPoints) {
            PointIds.add(userDeliveryPoint.getPointId());
        }
        Wrapper<OpDeliveryPoint> opDeliveryPointWrapper = new EntityWrapper<>();
        opDeliveryPointWrapper.in("id", PointIds);
        List<OpDeliveryPoint> opDeliveryPoints = deliveryPointService.selectList(opDeliveryPointWrapper);
        ArrayList<String> Points = new ArrayList<>();
        for (OpDeliveryPoint opDeliveryPoint : opDeliveryPoints) {
            Points.add(opDeliveryPoint.getCode());
        }
        Wrapper<OrderReleaseParamDTO> ew = new EntityWrapper<>();
        Map<String, Object> condition = page.getCondition();
        ew.in("origin_location_gid", Points);
        if (condition != null) {
            if (condition.containsKey("boundType") && Objects.nonNull(condition.get("boundType"))) {
                ew.eq("bound_type", condition.get("boundType"));
            }
            if (condition.containsKey("plateNo") && Objects.nonNull(condition.get("plateNo"))) {
                ew.like("plate_no", condition.get("plateNo").toString());
            }
            if (condition.containsKey("originLocationName") && Objects.nonNull(condition.get("originLocationName"))) {
                ew.like("origin_location_name", condition.get("originLocationName").toString());
            }
            if (condition.containsKey("destLocationName") && Objects.nonNull(condition.get("destLocationName"))) {
                ew.like("dest_location_name", condition.get("destLocationName").toString());
            }
            if (condition.containsKey("startDate") && Objects.nonNull(condition.get("startDate"))) {
                ew.ge("gmt_create", condition.get("startDate").toString());
            }
            if (condition.containsKey("endDate") && Objects.nonNull(condition.get("endDate"))) {
                ew.le("gmt_create", condition.get("endDate").toString());
            }
            if (condition.containsKey("vin") && Objects.nonNull(condition.get("vin"))) {
                String vin = condition.get("vin").toString();
                String[] split = vin.split(",");
                List<String> vins = Arrays.asList(split);
                ew.andNew().in("vin", vins).or().like("vin", vin);
            }
        }
        ew.ne("shipmentStatus", TableStatusEnum.STATUS_50.getCode());
        ew.orderBy("gmt_create");
        List<OrderReleaseParamDTO> dtoList = this.baseMapper.queryOrderReleaseList(page, ew);
        page.setRecords(dtoList);
        return page;
    }

    /**
     * 装车交验 - 查询列表
     */
    @Override
    public Page<ReleaseWithShipmentDTO> queryReleaseShipList(Page<ReleaseWithShipmentDTO> page) {
        if (page == null) {
            throw new BaseException("参数page不能为空");
        }
        Map<String, Object> condition = page.getCondition();
        if (condition == null || condition.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        Object key = condition.get("key");
        if (key == null || StringUtils.isBlank(key.toString())) {
            throw new BaseException("查询参数不能为空");
        }
        EntityWrapper<ReleaseWithShipmentDTO> ew = new EntityWrapper<>();
        ew.isNotNull("userId")
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .ne("status", TableStatusEnum.STATUS_50.getCode())
                .eq("userCode", loginUser.getCode())
                .andNew()
                .like("cus_order_no", key.toString())
                .or()
                .like("shipment_gid", key.toString())
                .or()
                .like("cus_waybill_no", key.toString())
                .or()
                .like("vin", key.toString())
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        List<ReleaseWithShipmentDTO> data = baseMapper.queryReleaseShipList(page, ew);
        if (CollectionUtils.isEmpty(data)) {
            throw new BaseException("为查询到对应发运数据");
        }
        page.setRecords(data);
        return page;
    }

    /**
     * 查询装车发运详细信息
     */
    @Override
    public ReleaseWithShipmentDTO getReleaseShipDetail(AppCommonQueryDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空");
        }
        Map<String, String> condition = dto.getCondition();
        if (condition == null || condition.isEmpty()) {
            throw new BaseException("查询参数不能为空");
        }
        String visitType = condition.get("visitType");
        String key = condition.get("key");
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        if (StringUtils.isBlank(visitType)) {
            throw new BaseException("访问方式不能为空");
        }
        if (StringUtils.isBlank(key)) {
            throw new BaseException("key不能为空");
        }
        if (InterfaceVisitTypeEnum.CLICK_TYPE.getCode().equals(visitType)) {
            ReleaseWithShipmentDTO detailClick = getDetailClick(key, loginUser);
            String send = exceptionToOTMService.isSend(detailClick.getVin(), detailClick.getOriginLocationName());
            if (StringUtils.isNotBlank(send)) detailClick.setIsCanSend(send);
            return detailClick;
        } else if (InterfaceVisitTypeEnum.SCAN_TYPE.getCode().equals(visitType)) {
            //支持qrCode 车架号  订单号 扫码
            ReleaseWithShipmentDTO detailScan = getDetailScan(key, loginUser);
            String send = exceptionToOTMService.isSend(detailScan.getVin(), detailScan.getOriginLocationName());
            if (StringUtils.isNotBlank(send)) detailScan.setIsCanSend(send);
            return detailScan;
        } else {
            throw new BaseException("不支持的访问方式");
        }
    }

    /**
     * 装车交验确认
     */
    @Override
    public OtmOrderRelease updateReleaseShip(AppCommonQueryDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空");
        }
        Map<String, String> condition = dto.getCondition();
        if (condition == null || condition.isEmpty()) {
            throw new BaseException("查询参数不能为空");
        }
        String key = condition.get("key");
        if (StringUtils.isBlank(key)) {
            throw new BaseException("key不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        OtmOrderRelease release = selectById(Long.valueOf(key));
        if (release == null) {
            throw new BaseException("未查询到Key" + key + "的运单信息");
        }
        if (TableStatusEnum.STATUS_WMS_HANDOVER.getCode().equals(release.getStatus())) {
            throw new BaseException("运单" + key + "已经交验,无需重复操作");
        }
        OtmOrderRelease oor = new OtmOrderRelease();
        oor.setStatus(TableStatusEnum.STATUS_WMS_HANDOVER.getCode());
        oor.setId(release.getId());
        updateById(oor);
        release.setStatus(TableStatusEnum.STATUS_WMS_HANDOVER.getCode());
        new Thread(() -> {
            StatusLog sl = new StatusLog();
            sl.setTableType(TableStatusEnum.STATUS_10.getCode());
            sl.setTableId(String.valueOf(release.getId()));
            sl.setStatus(TableStatusEnum.STATUS_WMS_HANDOVER.getCode());
            sl.setStatusName(TableStatusEnum.STATUS_WMS_HANDOVER.getCode());
            sl.setUserCreate(loginUser.getName());
            statusLogService.insert(sl);
        }).start();
        return release;
    }

    /**
     * 装车发运 - 模糊搜索查询
     */
    @Override
    public Page<ShipmentForShipDTO> queryShipList(Page<ShipmentForShipDTO> page) {
        if (page == null) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> condition = page.getCondition();
        if (condition == null || condition.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        Object status = condition.get("status");
        //指令号，板车车牌号
        Object key = condition.get("key");
        if (status == null || StringUtils.isBlank(status.toString())) {
            throw new BaseException("状态信息不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        baseMapper.updateSQLMode();
        EntityWrapper<ShipmentForShipDTO> ew = new EntityWrapper<>();
        ew.eq("userCode", loginUser.getCode()).ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode());
        if (StringUtils.isNotEmpty((String) key)) {
            ew.andNew().like("shipment_gid", key.toString()).or().like("plate_no", key.toString());
        }
        ew.orderBy("gmt_create", false)
                .orderBy("id", false)
                .orderBy("releaseId", false)
                .groupBy("shipment_gid");
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("status", status.toString());
        params.put("start", page.getOffsetCurrent());
        params.put("end", page.getSize());
        params.put("orderBy", "gmt_create desc,id desc");
        List<ShipmentForShipDTO> data = shipmentService.queryShipmentReleaseList(page, params, ew);
        ArrayList<String> gids = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(data)) {
            for (ShipmentForShipDTO dto : data) {
                gids.add(dto.getShipmentGid());
            }
            EntityWrapper<ShipmentForShipDTO> dataEW = new EntityWrapper<>();
            dataEW.in("shipment_gid", gids)
                    .eq("status", status.toString())
                    .ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                    .eq("is_ship", TableStatusEnum.STATUS_1.getCode());
            if (StringUtils.isNotEmpty((String) key)) {
                dataEW.andNew().like("shipment_gid", key.toString()).or().like("plate_no", key.toString());
            }
            dataEW.orderBy("gmt_create", false)
                    .orderBy("id", false)
                    .orderBy("releaseId", false);
            List<ShipmentForShipDTO> list = shipmentService.queryShipmentReleases(dataEW);

            EntityWrapper<ShipmentForShipDTO> ttlEW = new EntityWrapper<>();
            ttlEW.eq("userCode", loginUser.getCode()).ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                    .eq("is_ship", TableStatusEnum.STATUS_1.getCode());
            if (StringUtils.isNotEmpty((String) key)) {
                ttlEW.andNew().like("shipment_gid", key.toString()).or().like("plate_no", key.toString());
            }
            ttlEW.orderBy("gmt_create", false)
                    .orderBy("id", false)
                    .orderBy("releaseId", false);
            int total = shipmentService.countShipmentReleaseList(params, ttlEW);
            //调整未查询到数据后台不抛出异常
            if (CollectionUtils.isNotEmpty(list)) {
                for (ShipmentForShipDTO dto : list) {
                    List<OtmOrderRelease> releaseList = dto.getOtmOrderReleaseList();
                    if (CollectionUtils.isEmpty(releaseList)) {
                        continue;
                    }
                    dto.setShipCount(releaseList.size());
                    dto.setOriginLocationName(releaseList.get(0).getOriginLocationName());
                    dto.setDestLocationName(releaseList.get(0).getDestLocationName());
                    int count = 0;
                    for (OtmOrderRelease release : releaseList) {
                        //已交验后状态  算作已交验数据
                        if (TableStatusEnum.STATUS_WMS_HANDOVER.getCode().equals(release.getStatus())
                                || TableStatusEnum.STATUS_BS_DISPATCH.getCode().equals(release.getStatus())
                                || TableStatusEnum.STATUS_BS_ARRIVAL.getCode().equals(release.getStatus())
                                || TableStatusEnum.STATUS_BS_POD.getCode().equals(release.getStatus())
                                || TableStatusEnum.STATUS_OR_CLOSED.getCode().equals(release.getStatus())) {
                            count++;
                        }
                    }
                    dto.setHandoverCount(count);
                }
            }
            if(StringUtils.isNotEmpty((String)key)){
                page.setTotal(data.size());
            }else{
                page.setTotal(total);
            }
            page.setRecords(list);
        }
        return page;
    }


    /**
     * 装车发运 - 模糊搜索查询
     */
    @Override
    public Page<ShipmentRecordDTO> queryShipRecordList(Page<ShipmentRecordDTO> page) {
        //1、参数校验
        checkShopListParam(page);
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        //2、组装查询条件
        EntityWrapper<ShipmentRecordDTO> srdEw = new EntityWrapper<>();
        srdEw.eq("b.status", page.getCondition().get("releaseStatus"));
        srdEw.eq("e.id", loginUser.getId());
        Map<String, Object> condition = page.getCondition();
        if (condition != null && !condition.isEmpty()) {
            if (Objects.nonNull(condition.get("cusWaybillNo")) && StringUtils.isNotEmpty((String) condition.get("cusWaybillNo"))) {
                srdEw.eq("b.cus_waybill_no", condition.get("cusWaybillNo"));
            }
            if (Objects.nonNull(condition.get("cusOrderNo")) && StringUtils.isNotEmpty((String) condition.get("cusOrderNo"))) {
                srdEw.like("b.cus_order_no", condition.get("cusOrderNo").toString());
            }
            if (Objects.nonNull(condition.get("shipmentGid")) && StringUtils.isNotEmpty((String) condition.get("shipmentGid"))) {
                srdEw.like("b.shipment_gid", condition.get("shipmentGid").toString());
            }
            if (Objects.nonNull(condition.get("startDate")) && StringUtils.isNotEmpty((String) condition.get("startDate"))) {
                srdEw.ge("b.gmt_modified", condition.get("startDate").toString());
            }
            if (Objects.nonNull(condition.get("endDate")) && StringUtils.isNotEmpty((String) condition.get("endDate"))) {
                srdEw.le("b.gmt_modified", condition.get("endDate").toString());
            }
            if (Objects.nonNull(condition.get("vin")) && StringUtils.isNotEmpty((String) condition.get("vin"))) {
                String[] split = condition.get("vin").toString().split(",");
                List<String> vins = Arrays.asList(split);
                srdEw.andNew().in("b.vin", vins).or().like("b.vin", condition.get("vin").toString());
            }
        }
        srdEw.orderBy("b.gmt_modified", false);
        List<ShipmentRecordDTO> list = shipmentService.queryShipRecordList(page, srdEw);
        return page.setRecords(list);
    }

    @Override
    public void isRepeatShipment(List<String> tempList) {
        //1、查询是否已经确认发运
        for (String key : tempList) {
            Wrapper<OtmShipment> osEw = new EntityWrapper<>();
            osEw.eq("id", key);
            osEw.eq("status", TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            List<OtmShipment> otmShipments = otmShipmentService.selectList(osEw);
            if (CollectionUtils.isNotEmpty(otmShipments)) {
                OtmShipment otmShipment = otmShipments.get(0);
                Wrapper<OtmOrderRelease> oorParams = new EntityWrapper<>();
                oorParams.eq("shipment_gid", otmShipment.getShipmentGid());
                List<OtmOrderRelease> otmOrderReleases = otmOrderReleaseService.selectList(oorParams);
                if (CollectionUtils.isNotEmpty(otmOrderReleases)) {
                    String msg = otmOrderReleases.get(0).getVin();
                    throw new BaseException(901, "该发运记录的状态为【已发运】,车架号【" + msg + "】请勿重复发运");
                }
                throw new BaseException(901, "该发运记录的状态为【已发运】，请勿重复发运");
            }
        }
    }

    /**
     * 入参校验
     *
     * @param page 入参
     */
    private void checkShopListParam(Page<ShipmentRecordDTO> page) {
        if (page == null) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> condition = page.getCondition();
        String releaseStatus = (String) condition.get("releaseStatus");
        if (StringUtils.isEmpty(releaseStatus)) {
            throw new BaseException("【releaseStatus】参数不能为空");
        }
    }


    /**
     * 装车发运 -- 点击/扫码获取详情
     */
    @Override
    public ShipmentForShipDTO getShipDetail(AppCommonQueryDTO dto) {
        if (dto == null) {
            throw new BaseException("dto 参数不能为空");
        }
        Map<String, String> condition = dto.getCondition();
        if (null == condition || condition.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String key = condition.get("key");
        if (StringUtils.isBlank(key)) {
            throw new BaseException("指令Id不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (null == loginUser) {
            throw new BaseException("请登录后进行该操作");
        }
        EntityWrapper<ShipmentForShipDTO> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode())
                .eq("id", key)
                .eq("userCode", loginUser.getCode())
                .ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false)
                .orderBy("releaseId", false);
        List<ShipmentForShipDTO> list = shipmentService.getShipDetail(ew);
        if (CollectionUtils.isEmpty(list)) {
            throw new BaseException("未查询到Id" + key + "的指令信息");
        }
        if (list.size() > 1) {
            throw new BaseException("查询到Id" + key + "多条指令信息");
        }

        for (ShipmentForShipDTO shipmentForShipDTO : list) {
            List<OtmOrderRelease> otmOrderReleases = shipmentForShipDTO.getOtmOrderReleaseList();
            if (CollectionUtils.isNotEmpty(otmOrderReleases)) {
                shipmentForShipDTO.setOriginLocationName(otmOrderReleases.get(0).getOriginLocationName());
                shipmentForShipDTO.setDestLocationName(otmOrderReleases.get(0).getDestLocationName());
            }
        }
        return list.get(0);
    }

    /**
     * 装车发运 -- 确认发运
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ShipmentForShipDTO updateShip(AppCommonQueryDTO dto) {
        if (dto == null) {
            throw new BaseException("dto 参数不能为空");
        }
        Map<String, String> condition = dto.getCondition();
        if (null == condition || condition.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String key = condition.get("key");
        if (StringUtils.isBlank(key)) {
            throw new BaseException("指令Id不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (null == loginUser) {
            throw new BaseException("请登录后进行该操作");
        }
        //关联运单 启运地(多个发运地,需要多个发车点确认发运)
        EntityWrapper<ShipmentForShipDTO> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode())
                .eq("id", key)
                .eq("userCode", loginUser.getCode())
                .ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false)
                .orderBy("releaseId", false);
        List<ShipmentForShipDTO> list = shipmentService.getShipDetail(ew);
        if (CollectionUtils.isEmpty(list)) {
            throw new BaseException("未查询到对应的指令信息");
        }
        if (list.size() > 1) {
            throw new BaseException("查询到多条指令信息");
        }
        //增加判断头状态改为已发运时需要所有的运单都为已发运
        ShipmentForShipDTO shipDTO = list.get(0);
        //减少请求,直接响应前端界面设置值
        shipDTO.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
        //更新运单明细 状态为已发运
        List<OtmOrderRelease> releaseList = shipDTO.getOtmOrderReleaseList();
        ArrayList<StatusLog> insertLogs = Lists.newArrayList();
        for (OtmOrderRelease oor : releaseList) {
            Wrapper<ExceptionRegister> registerWrapper = new EntityWrapper<>();
            registerWrapper.eq("vin", oor.getVin())
                    .eq("otm_status", TableStatusEnum.STATUS_N.getCode());
            ExceptionRegister exceptionRegister = exceptionRegisterService.selectOne(registerWrapper);
            if (Objects.nonNull(exceptionRegister)) {
                throw new BaseException("车架号:" + oor.getVin() + "异常不发运,请联系调度!");
            }
            //更新明细状态
            OtmOrderRelease release = new OtmOrderRelease();
            release.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            release.setId(oor.getId());
            updateById(release);
            oor.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            StatusLog sl = new StatusLog();
            sl.setTableType(TableStatusEnum.STATUS_10.getCode());
            sl.setTableId(String.valueOf(release.getId()));
            sl.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setStatusName(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setUserCreate(loginUser.getName());
            insertLogs.add(sl);
            //发运  更新车辆为已出库
            ArrayList<String> status = Lists.newArrayList();
            status.add(TableStatusEnum.STATUS_10.getCode());
            status.add(TableStatusEnum.STATUS_20.getCode());
            EntityWrapper<OutboundNoticeLine> nlEW = new EntityWrapper<>();
            nlEW.eq("line_source_key", oor.getReleaseGid())
                    .in("status", status)
                    .orderBy("id", false);
            OutboundNoticeLine noticeLine = outboundNoticeLineService.selectOne(nlEW);
            if (noticeLine != null) {
                //调用方法出库
                outboundShipLineService.shipByNoticeLineId(noticeLine.getId(), PutAwayType.NOTICE_PUTAWAY, SourceSystem.AUTO);
                //调用方法修改备料状态
                prepareHeaderService.updatePrepareFinishByLineId(noticeLine.getId(), loginUser.getName());
            }

            //推送OTM/BMS
            this.toOtmBms(shipDTO, oor);
           /* new Thread(() -> {
                OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(release.getId()), oor.getReleaseGid(), InterfaceEventEnum.BS_OP_DELIVERY.getCode(),
                        oor.getShipmentGid(), "已发运回传OTM/BMS");
                //调整参数--BMS传递更多信息
                ShipParamDTO paramDTO = buildBMSShipParam(shipDTO, oor, otmEvent);
                String res = nodeExport.exportEventToOTMNew(paramDTO);
                integrationService.insertExportLogNew(String.valueOf(release.getId()), paramDTO, res, "已发运回传OTM/BMS", InterfaceEventEnum.BS_OP_DELIVERY.getCode());
            }).start();*/
        }
        //调整头发运状态 增加判断明细都为已发运
        String shipmentGid = shipDTO.getShipmentGid();
        ArrayList<String> neStatus = Lists.newArrayList(TableStatusEnum.STATUS_50.getCode(),
                TableStatusEnum.STATUS_BS_ARRIVAL.getCode(),
                TableStatusEnum.STATUS_BS_DISPATCH.getCode());
        EntityWrapper<OtmOrderRelease> oorEW = new EntityWrapper<>();
        oorEW.eq("shipment_gid", shipmentGid)
                .notIn("status", neStatus);
        int notShipCount = otmOrderReleaseService.selectCount(oorEW);
        if (notShipCount == 0) {
            this.modifyShipmentStatus(shipDTO.getId());
        }
        //statusLog 操作日志记录
        if (CollectionUtils.isNotEmpty(insertLogs)) {
            statusLogService.insertBatch(insertLogs);
        }
        return shipDTO;
    }

    private ShipParamDTO buildBMSShipParam(ShipmentForShipDTO shipDTO, OtmOrderRelease oor, OTMEvent otmEvent) {
        ShipParamDTO paramDTO = new ShipParamDTO();
        paramDTO.setPlateNo(shipDTO.getPlateNo());
        paramDTO.setTrailerNo(shipDTO.getTrailerNo());
        paramDTO.setDriverGid(shipDTO.getDriverGid());
        paramDTO.setProviderGid(shipDTO.getServiceProviderGid());
        paramDTO.setVin(oor.getVin());
        paramDTO.setVehicleName(oor.getStanVehicleType());
        paramDTO.setOriginProvince(oor.getOriginLocationProvince());
        paramDTO.setOriginCity(oor.getOriginLocationCity());
        paramDTO.setOriginCounty(oor.getOriginLocationCounty());
        paramDTO.setOriginAddr(oor.getOriginLocationAddress());
        paramDTO.setDestProvince(oor.getDestLocationProvince());
        paramDTO.setDestCity(oor.getDestLocationCity());
        paramDTO.setDestCounty(oor.getDestLocationCounty());
        paramDTO.setDestAddr(oor.getDestLocationAddress());
        paramDTO.setOriginCode(oor.getOriginLocationGid());
        paramDTO.setDestCode(oor.getDestLocationGid());
        paramDTO.setCusOrderNo(oor.getCusOrderNo());
        paramDTO.setCusWaybill(oor.getCusWaybillNo());
        paramDTO.setExportKey(otmEvent.getExportKey());
        paramDTO.setCallBackUrl(otmEvent.getCallBackUrl());
        paramDTO.setEventType(otmEvent.getEventType());
        paramDTO.setOccurDate(otmEvent.getOccurDate());
        paramDTO.setRecdDate(otmEvent.getRecdDate());
        paramDTO.setSort(otmEvent.getSort());
        paramDTO.setDescribe(otmEvent.getDescribe());
        paramDTO.setOrderReleaseId(otmEvent.getOrderReleaseId());
        paramDTO.setShipmentId(otmEvent.getShipmentId());
        paramDTO.setQrCode(otmEvent.getQrCode());
        paramDTO.setShipTime(String.valueOf(new Date().getTime()));
        return paramDTO;
    }

    private ReleaseWithShipmentDTO getDetailScan(String key, User loginUser) {
        EntityWrapper<ReleaseWithShipmentDTO> ew = new EntityWrapper<>();
        ew.isNotNull("userId")
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .ne("status", TableStatusEnum.STATUS_50.getCode())
                .eq("userCode", loginUser.getCode())
                .andNew()
                .eq("qr_code", key)
                .or()
                .eq("vin", key)
                .or()
                .eq("cus_order_no", key)
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        List<ReleaseWithShipmentDTO> data = baseMapper.getReleaseShipDetail(ew);
        if (CollectionUtils.isEmpty(data)) {
            throw new BaseException("为查询到对应发运数据");
        }
        if (data.size() > 1) {
            throw new BaseException("查询到key:" + key + "多条信息");
        }
        return data.get(0);
    }

    private ReleaseWithShipmentDTO getDetailClick(String key, User loginUser) {
        EntityWrapper<ReleaseWithShipmentDTO> ew = new EntityWrapper<>();
        ew.isNotNull("userId")
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .ne("status", TableStatusEnum.STATUS_50.getCode())
                .eq("userCode", loginUser.getCode())
                .eq("id", key)
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        List<ReleaseWithShipmentDTO> data = baseMapper.getReleaseShipDetail(ew);
        if (CollectionUtils.isEmpty(data)) {
            throw new BaseException("为查询到对应发运数据");
        }
        if (data.size() > 1) {
            throw new BaseException("查询到key:" + key + "多条信息");
        }
        return data.get(0);
    }

    @Override
    public void updateVin(AppCommonQueryDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空");
        }
        Map<String, String> condition = dto.getCondition();
        if (null == condition || condition.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        if (!condition.containsKey("key") || StringUtils.isBlank(condition.get("key"))) {
            throw new BaseException("指令id不能为空");
        }
        if (!condition.containsKey("vin") || StringUtils.isBlank(condition.get("vin"))) {
            throw new BaseException("车架号不能为空");
        }

        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("未查询到登录用户");
        }
        if (StringUtils.isBlank(loginUser.getCode())) {
            throw new BaseException("用户编码不能为空");
        }
        OtmOrderRelease orderRelease = new OtmOrderRelease();
        orderRelease.setId(Long.valueOf(condition.get("key")));
        orderRelease.setVin(condition.get("vin"));
        baseMapper.updateById(orderRelease);
        OtmOrderRelease orderReleaseResult = baseMapper.selectById(orderRelease.getId());
        // 车架号回传OTM
        sendVinBindOTM(String.valueOf(orderReleaseResult.getId()), orderReleaseResult.getVin(), orderReleaseResult);
    }

    @Override
    public Page<VechielModelDTO> queryCarModelInfo(Page<VechielModelDTO> page) {
        //调用lisa-integration系统获取车型信息,通过httpclient的post方式调用
        String result = HttpClientUtil.postJson(properties.getIntegrationhost() + InterfaceAddrEnum.VEHICLE_URL.getAddress(), null, JSONObject.toJSONString(page), properties.getSocketTimeOut());
        if (StringUtils.isNotEmpty(result)) {
            RestfulResponse<Page<VechielModelDTO>> restfulResponse = JSON.parseObject(result,
                    new TypeReference<RestfulResponse<Page<VechielModelDTO>>>() {
                    });
            if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                return restfulResponse.getData();
            }
        }
        return null;
    }

    @Override
    public void updateVehicleInfo(AppCommonQueryDTO conditions) {
        //1、修改车型信息参数判断
        Map<String, String> condition = this.setUpdateVehicleInfoParams(conditions);

        //2、登录校验
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("未查询到登录用户");
        }
        if (StringUtils.isBlank(loginUser.getCode())) {
            throw new BaseException("用户编码不能为空");
        }

        //3、修改车型
        OtmOrderRelease otmOrderRelease = new OtmOrderRelease();
        otmOrderRelease.setIsModVehicle("Y");
        otmOrderRelease.setId(Long.valueOf(condition.get("key")));
        otmOrderRelease.setModifiedVehicleType(condition.get("modifiedVehicleType"));
        otmOrderRelease.setModifiedVehicleUser(loginUser.getName());
        otmOrderRelease.setModifiedVehicleTime(new Date());
        baseMapper.updateById(otmOrderRelease);

        //4、同步OTM
        boolean syncOTMFlag = this.vehicleToOTM(otmOrderRelease.getId(), condition.get("modifiedVehicleCode"));
        if (StringUtils.isEmpty(condition.get("modifiedVehicleCode"))) {
            throw new BaseException(99, "修改车型的code为空，请重新确认!");
        }
        LOGGER.info("修改车型同步OTM返回结果syncOTMFlag{} ", syncOTMFlag);
    }

    /**
     * 车型同步OTM
     *
     * @param id
     */
    private boolean vehicleToOTM(Long id, String modifiedVehicleCode) {
        OtmOrderRelease orderReleaseResult = baseMapper.selectById(id);
        Map<String, String> param = new HashMap<>();
        //客户 可不传
        param.put("customer", "");
        //OMS订单号 必传
        param.put("orderReleaseXid", orderReleaseResult.getReleaseGid());
        //车架号 必传
        param.put("vin", orderReleaseResult.getVin());
        //改装后车型 必传
        param.put("stdVechile", modifiedVehicleCode);
        param.put("shipmentGid", orderReleaseResult.getShipmentGid());
        String result = HttpClientUtil.postJson(properties.getIntegrationhost() + InterfaceAddrEnum.VEHICLE_SYNS_TOOTM_URL.getAddress(), null, JSONObject.toJSONString(param), properties.getSocketTimeOut());
        if (StringUtils.isNotEmpty(result)) {
            RestfulResponse<Map<String, Object>> restfulResponse = JSON.parseObject(result, new TypeReference<RestfulResponse<Map<String, Object>>>() {
            });
            if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                return true;
            }
        }
        return false;
    }

    private Map<String, String> setUpdateVehicleInfoParams(AppCommonQueryDTO conditions) {
        Map<String, String> condition = conditions.getCondition();
        if (Objects.isNull(condition)) {
            throw new BaseException(99, "入参不能为空");
        }
        String keyId = condition.get("key");
        if (StringUtils.isEmpty(keyId)) {
            throw new BaseException(99, "主键id不能为空");
        }
        String vehicleType = condition.get("modifiedVehicleType");
        if (StringUtils.isEmpty(vehicleType)) {
            throw new BaseException(99, "车型不能为空");
        }
        return condition;
    }


    private void sendVinBindOTM(String id, String vin, OtmOrderRelease orderReleaseResult) {
        new Thread(() -> {
            OTMEvent event = integrationService.getOtmEvent(
                    id,
                    orderReleaseResult.getReleaseGid(),
                    InterfaceEventEnum.BINDING_CODE.getCode(),
                    orderReleaseResult.getShipmentGid(),
                    "绑定车架号信息回传OTM");
            event.setVin(vin);
            String res = nodeExport.exportEventToOTM(event);
            if (StringUtils.isNotBlank(res)) {
                integrationService.insertExportLog(
                        id,
                        event,
                        res,
                        "绑定车架号信息回传OTM",
                        InterfaceEventEnum.BINDING_CODE.getCode());
            }
        }).start();
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void releaseDespatch (Map<String, String> param) {
        //1、判断入参是否为空，入参为运单表的id，多个运单发运以英文逗号“,”分开
        String releaseIds = param.get("key");
        if(StringUtils.isEmpty(releaseIds)){
            throw new BaseException("运单ID【releaseIds】为空，请重新确认！");
        }

        //2、登录状态校验
        User loginUser = userService.getLoginUser();
        if (null == loginUser) {
            throw new BaseException("请登录后进行该操作");
        }

        //3、根据运单id查询运单信息
        List<OtmReleaseForShipDTO> releaseList = this.queryOtmReleaseList(releaseIds,loginUser.getCode());

        //4、更新运单信息 状态为已发运
        ArrayList<StatusLog> statusLogs = this.modifiedReleaseStatus(releaseList,loginUser);

        //5、statusLog 操作日志记录
        if (CollectionUtils.isNotEmpty(statusLogs)) {
            statusLogService.insertBatch(statusLogs);
        }
    }

    /**
     * 更新运单状态为已发运
     *  @param releaseList 运单列表
     * @param loginUser   登录用户信息
     */
    private ArrayList<StatusLog> modifiedReleaseStatus (List<OtmReleaseForShipDTO> releaseList, User loginUser) {
        ArrayList<StatusLog> insertLogs = Lists.newArrayList();
        for (OtmReleaseForShipDTO oor : releaseList) {
            //1、是否标记为异常发运,标记为异常发运的车架号不能发运
            this.isExceptionDespatch(oor.getVin());

            //2、是否为已发运
            this.isShip(oor);

            //3、更新运单状态
            OtmOrderRelease release = new OtmOrderRelease();
            release.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            release.setId(oor.getId());
            updateById(release);
            oor.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());

            //4、新增状态日志表
            StatusLog sl = new StatusLog();
            sl.setTableType(TableStatusEnum.STATUS_10.getCode());
            sl.setTableId(String.valueOf(release.getId()));
            sl.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setStatusName(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setUserCreate(loginUser.getName());
            insertLogs.add(sl);

            //5、更新出库通知单车辆为已出库
            this.modifyOutbound(oor,loginUser.getName());

            //6、已发运的数据同步到BMS/OTM
            this.passBackBMSAndOTM(oor);
        }


        return insertLogs;
    }

    private void isShip (OtmReleaseForShipDTO oor) {
        Wrapper<OtmOrderRelease> param = new EntityWrapper<>();
        param.eq("id", oor.getId());
        param.eq("vin", oor.getVin());
        param.eq("status", TableStatusEnum.STATUS_BS_DISPATCH.getCode());
        int count = selectCount(param);
        if (count == 1) {
            throw new BaseException("运单" + oor.getReleaseGid() + " 对应的车架号 " + oor.getVin() + "已发运，无需重复发运");
        }
    }

    /**
     * 更改指令为已发运
     *
     * @param id 指令主键id
     */
    private void modifyShipmentStatus (Long id) {
        //明细已经全部发运/运抵
        OtmShipment os = new OtmShipment();
        os.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
        os.setId(id);
        shipmentService.updateById(os);
    }

    /**
     * 更改指令状态并发送给OTM/BMS
     *
     * @param oor
     */
    private void passBackBMSAndOTM (OtmReleaseForShipDTO oor) {
        //1、根据运单号获取指令信息
        Wrapper<OtmShipment> ew = new EntityWrapper<>();
        ew.eq("shipment_gid", oor.getShipmentGid());
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        OtmShipment otmShipments = otmShipmentService.selectOne(ew);
        LOGGER.info(" 回传OTM/BMS根据{}查询的指令信息为：{}", oor.getReleaseGid(), otmShipments);
        if (null == otmShipments) {
            throw new BaseException("指令信息为空，请联系相关人员查询问题原因");
        }
        if (TableStatusEnum.STATUS_BS_DISPATCH.equals(otmShipments.getStatus())) {
            throw new BaseException("运单" + oor.getReleaseGid() + "信息对应的指令状态为已发运，请联系相关人员查询问题原因");
        }

        //2、设置回传信息给OTM/BMS的参数
        ShipmentForShipDTO shipmentForShipDTO = new ShipmentForShipDTO();
        shipmentForShipDTO.setDriverGid(otmShipments.getDriverGid());
        shipmentForShipDTO.setServiceProviderGid(otmShipments.getServiceProviderGid());
        shipmentForShipDTO.setPlateNo(otmShipments.getPlateNo());
        shipmentForShipDTO.setTrailerNo(otmShipments.getTrailerNo());
        OtmOrderRelease otmRelease = this.setReleaseInfo(oor);

        //3、调整头发运状态 增加判断明细都为已发运
        ArrayList<String> neStatus = Lists.newArrayList(TableStatusEnum.STATUS_50.getCode(), TableStatusEnum.STATUS_BS_ARRIVAL.getCode(), TableStatusEnum.STATUS_BS_DISPATCH.getCode());
        EntityWrapper<OtmOrderRelease> oorEW = new EntityWrapper<>();
        oorEW.eq("shipment_gid", oor.getShipmentGid()).notIn("status", neStatus);
        int notShipCount = otmOrderReleaseService.selectCount(oorEW);
        if (notShipCount == 0) {
            this.modifyShipmentStatus(otmShipments.getId());
        }

        //4、回传信息给OTM/BMS
        this.toOtmBms(shipmentForShipDTO, otmRelease);
    }

    private OtmOrderRelease setReleaseInfo (OtmReleaseForShipDTO oor) {
        OtmOrderRelease otmOrderRelease = new OtmOrderRelease();
        otmOrderRelease.setVin(oor.getVin());
        otmOrderRelease.setStanVehicleType(oor.getStanVehicleType());
        otmOrderRelease.setOriginLocationProvince(oor.getOriginLocationProvince());
        otmOrderRelease.setOriginLocationCity(oor.getOriginLocationCity());
        otmOrderRelease.setOriginLocationCounty(oor.getOriginLocationCounty());
        otmOrderRelease.setOriginLocationAddress(oor.getOriginLocationAddress());
        otmOrderRelease.setDestLocationProvince(oor.getDestLocationProvince());
        otmOrderRelease.setDestLocationCity(oor.getDestLocationCity());
        otmOrderRelease.setDestLocationCounty(oor.getDestLocationCounty());
        otmOrderRelease.setDestLocationAddress(oor.getDestLocationAddress());
        otmOrderRelease.setOriginLocationGid(oor.getOriginLocationGid());
        otmOrderRelease.setDestLocationGid(oor.getDestLocationGid());
        otmOrderRelease.setCusOrderNo(oor.getCusOrderNo());
        otmOrderRelease.setCusWaybillNo(oor.getCusWaybillNo());
        otmOrderRelease.setId(oor.getId());
        otmOrderRelease.setShipmentGid(oor.getShipmentGid());
        otmOrderRelease.setReleaseGid(oor.getReleaseGid());
        return otmOrderRelease;
    }

    /**
     *
     * @param shipmentForShipDTO 指令信息
     * @param oor 运单信息
     */
    private void toOtmBms (ShipmentForShipDTO shipmentForShipDTO, OtmOrderRelease oor) {
        new Thread(() -> {
            OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(oor.getId()), oor.getReleaseGid(),InterfaceEventEnum.BS_OP_DELIVERY.getCode(),
                    oor.getShipmentGid(),"已发运回传OTM/BMS");
            //调整参数--BMS传递更多信息
            ShipParamDTO paramDTO = buildBMSShipParam(shipmentForShipDTO, oor, otmEvent);
            String res = nodeExport.exportEventToOTMNew(paramDTO);
            integrationService.insertExportLogNew(String.valueOf(oor.getId()), paramDTO, res,"已发运回传OTM/BMS", InterfaceEventEnum.BS_OP_DELIVERY.getCode());
        }).start();
    }

    /**
     * 更新出库通知单状态
     *
     * @param oor  运单信息
     * @param loginName 登录用户名
     */
    private void modifyOutbound (OtmReleaseForShipDTO oor, String loginName) {
        ArrayList<String> status = Lists.newArrayList();
        status.add(TableStatusEnum.STATUS_10.getCode());
        status.add(TableStatusEnum.STATUS_20.getCode());
        EntityWrapper<OutboundNoticeLine> nlEW = new EntityWrapper<>();
        nlEW.eq("line_source_key", oor.getReleaseGid()).in("status", status).orderBy("id", false);
        OutboundNoticeLine noticeLine = outboundNoticeLineService.selectOne(nlEW);
        if (noticeLine != null) {
            //调用方法出库
            outboundShipLineService.shipByNoticeLineId(noticeLine.getId(), PutAwayType.NOTICE_PUTAWAY, SourceSystem.AUTO);
            //调用方法修改备料状态
            prepareHeaderService.updatePrepareFinishByLineId(noticeLine.getId(), loginName);
        }
    }

    /**
     * 是否异常发运
     *
     * @param re_vin 车架号
     */
    private void isExceptionDespatch (String re_vin) {
        Wrapper<ExceptionRegister> registerWrapper = new EntityWrapper<>();
        registerWrapper.eq("vin", re_vin).eq("otm_status", TableStatusEnum.STATUS_N.getCode());
        ExceptionRegister exceptionRegister = exceptionRegisterService.selectOne(registerWrapper);
        if (Objects.nonNull(exceptionRegister)) {
            throw new BaseException("车架号:" + re_vin + "异常不发运,请联系调度!");
        }

    }

    /**
     * 根据运单id获取运单信息，并查询状态是否为已发运，已发运的运单无需再次发运
     *
     * @param releaseIds 运单id
     * @return 运单信息
     * @deprecated is_ship 需发运(0:否,1:是)
     */
    private List<OtmReleaseForShipDTO> queryOtmReleaseList (String releaseIds, String userCode) {
        //关联运单 启运地(多个发运地,需要多个发车点确认发运)
        EntityWrapper<OtmReleaseForShipDTO> ew = new EntityWrapper<>();
        ew.in("releaseId", releaseIds.split(","))
                .eq("userCode", userCode)
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                .orderBy("re_gmt_modify", false)
                .orderBy("releaseId", false);
        List<OtmReleaseForShipDTO> list = otmShipmentMapper.queryOtmReleaseList(ew);
        if (CollectionUtils.isEmpty(list)) {
            throw new BaseException("未查询到对应的运单信息");
        }
        return list;
    }
}
