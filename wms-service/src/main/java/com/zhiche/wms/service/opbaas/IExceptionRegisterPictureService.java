package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegisterPicture;
import com.zhiche.wms.dto.opbaas.resultdto.PictureWithExcpDescDTO;

import java.util.List;

/**
 * <p>
 * 异常图片 服务类
 * </p>
 *
 * @author user
 * @since 2018-06-04
 */
public interface IExceptionRegisterPictureService extends IService<ExceptionRegisterPicture> {

    List<PictureWithExcpDescDTO> listPicsAndDesc(EntityWrapper<ExceptionRegisterPicture> picEW);

}
