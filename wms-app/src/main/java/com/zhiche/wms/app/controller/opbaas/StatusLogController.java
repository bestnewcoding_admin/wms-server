package com.zhiche.wms.app.controller.opbaas;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 状态日志 前端控制器
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-11
 */
@Controller
@RequestMapping("/domain.model/statusLog")
public class StatusLogController {

}

