package com.zhiche.wms.app.controller.stock;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceVisitTypeEnum;
import com.zhiche.wms.domain.model.log.CancelStoreLog;
import com.zhiche.wms.dto.opbaas.paramdto.AppCommonQueryDTO;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.VechielModelDTO;
import com.zhiche.wms.dto.stock.StockDTO;
import com.zhiche.wms.service.stock.AutoInAndOutBoundService;
import com.zhiche.wms.service.stock.IStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by hxh on 2018/8/2.
 */
@Api(value = "stock-API", description = "库位调整操作接口")
@RestController
@RequestMapping(value = "/stock", produces = MediaType.APPLICATION_JSON_VALUE)
public class StockController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private IStockService stockService;
    @Autowired
    private AutoInAndOutBoundService autoInAndOutBoundService;

    @PostMapping(value = "/queryPage")
    @ResponseBody
    public RestfulResponse<Page<StockDTO>> queryPage(AppQueryVo page) {
        LOGGER.info("Controller:stock/queryPage data: {}", page.toString());
        RestfulResponse<Page<StockDTO>> result = new RestfulResponse<>(0, "success");
        try {
            Page<StockDTO> stockDTOPage = new Page<StockDTO>();
            Map<String, Object> map = new HashMap<>();
            map.put("storeHouseId", page.getHouseId());
            map.put("lotNo1", page.getKey());
            stockDTOPage.setCondition(map);
            stockDTOPage.setSize(page.getSize());
            stockDTOPage.setCurrent(page.getCurrent());
            Page<StockDTO> resultPage = stockService.queryPageStock(stockDTOPage);
            result.setData(resultPage);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    @PostMapping(value = "/getStockInfo")
    @ResponseBody
    public RestfulResponse<StockDTO> getStockInfo(AppQueryVo page) {
        LOGGER.info("Controller:stock/queryPage data: {}", page.toString());
        RestfulResponse<StockDTO> result = new RestfulResponse<>(0, "success");
        try {
            Page<StockDTO> stockDTOPage = new Page<>();
            Page<StockDTO> resultPage = null;
            if (InterfaceVisitTypeEnum.CLICK_TYPE.getCode().equals(page.getVisitType())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeHouseId", page.getHouseId());
                map.put("stockId", page.getKey());
                stockDTOPage.setCondition(map);
                resultPage = stockService.queryPageStock(stockDTOPage);
            } else if (InterfaceVisitTypeEnum.SCAN_TYPE.getCode().equals(page.getVisitType())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeHouseId", page.getHouseId());
                map.put("key", page.getKey());
                stockDTOPage.setCondition(map);
                resultPage = stockService.queryPageStockByQrCode(stockDTOPage);
            }
            if (resultPage != null
                    && Objects.nonNull(resultPage.getRecords())
                    && resultPage.getRecords().size() > 0) {
                result.setData(resultPage.getRecords().get(0));
            } else {
                result.setCode(-1);
                result.setMessage("无相关数据");
            }
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 更新库位
     */
    @PostMapping("/updateStockLocation")
    @ResponseBody
    public RestfulResponse<Object> updateStockLocation(CommonConditionParamDTO dto) {
        stockService.updateStockLocation(dto.getCondition());
        return new RestfulResponse<>(0, "调整成功");
    }


    /**
     * 备料退库记录查询
     */
    @PostMapping(value = "/queryCancelStoreLog")
    @ApiOperation(value = "备料退库记录查询")
    public RestfulResponse<Page<CancelStoreLog>> queryCancelStoreLog (Page<CancelStoreLog> page) {
        LOGGER.info("/queryCancelStoreLog （备料退库记录查询） params : {} " ,page);
        RestfulResponse<Page<CancelStoreLog>> result = new RestfulResponse<>(0, "success", null);
        try {
            Page<CancelStoreLog> cancelStoreLogPage = stockService.queryCancelStoreLog(page);
            result.setData(cancelStoreLogPage);
        }catch (Exception e) {
            LOGGER.error("/queryCancelStoreLog （备料退库记录查询）  ERROR... ", e);
            result = new RestfulResponse(99, "failed", null);
        }
        return result;
    }

    /**
     * 备料退库记录详情查询
     */
    @PostMapping(value = "/queryCancelStoreDetail")
    @ApiOperation(value = "备料退库记录详情查询")
    public RestfulResponse<CancelStoreLog> queryCancelStoreDetail (AppQueryVo cancelStoreLogParam) {
        LOGGER.info("/queryCancelStoreDetail （备料退库记录详情查询） params : {} " ,cancelStoreLogParam);
        RestfulResponse<CancelStoreLog> result = new RestfulResponse<>(0, "success", null);
        try {
            CancelStoreLog cancelStoreLogPage = stockService.queryCancelStoreDetail(cancelStoreLogParam.getHouseId(),cancelStoreLogParam.getKey(),cancelStoreLogParam.getVisitType());
            result.setData(cancelStoreLogPage);
        }catch (Exception e) {
            LOGGER.error("/queryCancelStoreDetail （备料退库记录详情查询）  ERROR... ", e);
            result = new RestfulResponse(99, "failed", null);
        }
        return result;
    }

    /**
     * 司机领取退库任务
     */
    @PostMapping(value = "/driverReceiveTask")
    @ApiOperation(value = "司机领取退库任务")
    public RestfulResponse driverReceiveTask (CommonConditionParamDTO dto) {
        LOGGER.info("/queryCancelStoreLog （司机领取退库任务） params : {} " , dto.getCondition());
        RestfulResponse<String> result = new RestfulResponse<>(0, "success", null);
        try {
            Map<String,String> condition = dto.getCondition();
            if(Objects.isNull(condition)){
                throw new BaseException(99,"请检查入参！");
            }
             stockService.driverReceiveTask(condition);
        }catch (BaseException be){
            LOGGER.error("/driverReceiveTask （司机领取退库任务）  ERROR... ", be);
            result = new RestfulResponse(be.getCode(), be.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("/driverReceiveTask （司机领取退库任务）  ERROR... ", e);
            result = new RestfulResponse(99, "failed", null);
        }
        return result;
    }

}
