package com.zhiche.wms.app.controller.sys;

import com.zhiche.wms.domain.model.sys.AppVersion;
import com.zhiche.wms.dto.base.ResultDTOWithPagination;
import com.zhiche.wms.service.sys.IAppVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/version")
public class AppConfigController {

    @Autowired
    private IAppVersionService versionService;

    @RequestMapping("/android/latest")
    public ResultDTOWithPagination<AppVersion> updateAndroidApp() {
        AppVersion lastVersion = versionService.getLastVersionAndroid();
        return new ResultDTOWithPagination<>(true, lastVersion, 0, "查询成功", null);
    }

    @RequestMapping("/ios/latest")
    public ResultDTOWithPagination<AppVersion> updateIosApp() {
        AppVersion lastVersion = versionService.getLastVersionIos();
        return new ResultDTOWithPagination<>(true, lastVersion, 0, "查询成功", null);
    }

}
