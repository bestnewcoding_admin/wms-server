package com.zhiche.wms.app.vo;

import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.domain.model.sys.Permission;
import com.zhiche.wms.domain.model.sys.User;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhaoguixin on 2018/6/23.
 */
public class UserVo extends User implements Serializable{

    private static final long serialVersionUID = 5727736792068473446L;

    private List<Permission> permissions;

    private List<Storehouse> storehouses;

    private List<OpDeliveryPoint> opDeliveryPoints;


    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public List<Storehouse> getStorehouses() {
        return storehouses;
    }

    public void setStorehouses(List<Storehouse> storehouses) {
        this.storehouses = storehouses;
    }

    public List<OpDeliveryPoint> getOpDeliveryPoints() {
        return opDeliveryPoints;
    }

    public void setOpDeliveryPoints(List<OpDeliveryPoint> opDeliveryPoints) {
        this.opDeliveryPoints = opDeliveryPoints;
    }
}
