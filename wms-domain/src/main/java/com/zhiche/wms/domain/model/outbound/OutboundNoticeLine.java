package com.zhiche.wms.domain.model.outbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 出库通知明细
 * </p>
 *
 * @author qichao
 * @since 2018-06-13
 */
@TableName("w_outbound_notice_line")
public class OutboundNoticeLine extends Model<OutboundNoticeLine> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 入库通知单头键
     */
    @TableField("header_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long headerId;
    /**
     * 序号
     */
    private String seq;
    /**
     * 行来源唯一键
     */
    @TableField("line_source_key")
    private String lineSourceKey;
    /**
     * 行来源单据号
     */
    @TableField("line_source_no")
    private String lineSourceNo;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 日志行ID
     */
    @TableField("log_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logLineId;
    /**
     * 物料ID
     */
    @TableField("materiel_id")
    private String materielId;
    /**
     * 物料代码
     */
    @TableField("materiel_code")
    private String materielCode;
    /**
     * 物料名称
     */
    @TableField("materiel_name")
    private String materielName;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计数量
     */
    @TableField("expect_qty")
    private BigDecimal expectQty;
    /**
     * 预计净重
     */
    @TableField("expect_net_weight")
    private BigDecimal expectNetWeight;
    /**
     * 预计毛重
     */
    @TableField("expect_gross_weight")
    private BigDecimal expectGrossWeight;
    /**
     * 预计体积
     */
    @TableField("expect_gross_cubage")
    private BigDecimal expectGrossCubage;
    /**
     * 预计件数
     */
    @TableField("expect_packed_count")
    private BigDecimal expectPackedCount;
    /**
     * 出库数量
     */
    @TableField("outbound_qty")
    private BigDecimal outboundQty;
    /**
     * 出库净重
     */
    @TableField("outbound_net_weight")
    private BigDecimal outboundNetWeight;
    /**
     * 出库毛重
     */
    @TableField("outbound_gross_weight")
    private BigDecimal outboundGrossWeight;
    /**
     * 出库体积
     */
    @TableField("outbound_gross_cubage")
    private BigDecimal outboundGrossCubage;
    /**
     * 出库件数
     */
    @TableField("outbound_packed_count")
    private BigDecimal outboundPackedCount;
    /**
     * 批号0
     */
    @TableField("lot_no0")
    private String lotNo0;
    /**
     * 批号1
     */
    @TableField("lot_no1")
    private String lotNo1;
    /**
     * 批号2
     */
    @TableField("lot_no2")
    private String lotNo2;
    /**
     * 批号3
     */
    @TableField("lot_no3")
    private String lotNo3;
    /**
     * 批号4
     */
    @TableField("lot_no4")
    private String lotNo4;
    /**
     * 批号5
     */
    @TableField("lot_no5")
    private String lotNo5;
    /**
     * 批号6
     */
    @TableField("lot_no6")
    private String lotNo6;
    /**
     * 批号7
     */
    @TableField("lot_no7")
    private String lotNo7;
    /**
     * 批号8
     */
    @TableField("lot_no8")
    private String lotNo8;
    /**
     * 批号9
     */
    @TableField("lot_no9")
    private String lotNo9;
    /**
     * 状态(10:未入库,20:部分入库,30:全部入库,40:关闭入库,40:取消)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 目的地-省
     */
    @TableField("dest_location_province")
    private String destLocationProvince;
    /**
     * 目的地-市
     */
    @TableField("dest_location_city")
    private String destLocationCity;
    /**
     * 目的地-区
     */
    @TableField("dest_location_county")
    private String destLocationCounty;
    /**
     * 目的地-详细地址
     */
    @TableField("dest_location_detail")
    private String destLocationDetail;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getLineSourceKey() {
        return lineSourceKey;
    }

    public void setLineSourceKey(String lineSourceKey) {
        this.lineSourceKey = lineSourceKey;
    }

    public String getLineSourceNo() {
        return lineSourceNo;
    }

    public void setLineSourceNo(String lineSourceNo) {
        this.lineSourceNo = lineSourceNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public Long getLogLineId() {
        return logLineId;
    }

    public void setLogLineId(Long logLineId) {
        this.logLineId = logLineId;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectQty() {
        return expectQty;
    }

    public void setExpectQty(BigDecimal expectQty) {
        this.expectQty = expectQty;
    }

    public BigDecimal getExpectNetWeight() {
        return expectNetWeight;
    }

    public void setExpectNetWeight(BigDecimal expectNetWeight) {
        this.expectNetWeight = expectNetWeight;
    }

    public BigDecimal getExpectGrossWeight() {
        return expectGrossWeight;
    }

    public void setExpectGrossWeight(BigDecimal expectGrossWeight) {
        this.expectGrossWeight = expectGrossWeight;
    }

    public BigDecimal getExpectGrossCubage() {
        return expectGrossCubage;
    }

    public void setExpectGrossCubage(BigDecimal expectGrossCubage) {
        this.expectGrossCubage = expectGrossCubage;
    }

    public BigDecimal getExpectPackedCount() {
        return expectPackedCount;
    }

    public void setExpectPackedCount(BigDecimal expectPackedCount) {
        this.expectPackedCount = expectPackedCount;
    }

    public BigDecimal getOutboundQty() {
        return outboundQty;
    }

    public void setOutboundQty(BigDecimal outboundQty) {
        this.outboundQty = outboundQty;
    }

    public BigDecimal getOutboundNetWeight() {
        return outboundNetWeight;
    }

    public void setOutboundNetWeight(BigDecimal outboundNetWeight) {
        this.outboundNetWeight = outboundNetWeight;
    }

    public BigDecimal getOutboundGrossWeight() {
        return outboundGrossWeight;
    }

    public void setOutboundGrossWeight(BigDecimal outboundGrossWeight) {
        this.outboundGrossWeight = outboundGrossWeight;
    }

    public BigDecimal getOutboundGrossCubage() {
        return outboundGrossCubage;
    }

    public void setOutboundGrossCubage(BigDecimal outboundGrossCubage) {
        this.outboundGrossCubage = outboundGrossCubage;
    }

    public BigDecimal getOutboundPackedCount() {
        return outboundPackedCount;
    }

    public void setOutboundPackedCount(BigDecimal outboundPackedCount) {
        this.outboundPackedCount = outboundPackedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getDestLocationProvince() {
        return destLocationProvince;
    }

    public void setDestLocationProvince(String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity() {
        return destLocationCity;
    }

    public void setDestLocationCity(String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty() {
        return destLocationCounty;
    }

    public void setDestLocationCounty(String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationDetail() {
        return destLocationDetail;
    }

    public void setDestLocationDetail(String destLocationDetail) {
        this.destLocationDetail = destLocationDetail;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OutboundNoticeLine{" +
                ", id=" + id +
                ", headerId=" + headerId +
                ", seq=" + seq +
                ", lineSourceKey=" + lineSourceKey +
                ", lineSourceNo=" + lineSourceNo +
                ", ownerId=" + ownerId +
                ", ownerOrderNo=" + ownerOrderNo +
                ", logLineId=" + logLineId +
                ", materielId=" + materielId +
                ", materielCode=" + materielCode +
                ", materielName=" + materielName +
                ", uom=" + uom +
                ", expectQty=" + expectQty +
                ", expectNetWeight=" + expectNetWeight +
                ", expectGrossWeight=" + expectGrossWeight +
                ", expectGrossCubage=" + expectGrossCubage +
                ", expectPackedCount=" + expectPackedCount +
                ", outboundQty=" + outboundQty +
                ", outboundNetWeight=" + outboundNetWeight +
                ", outboundGrossWeight=" + outboundGrossWeight +
                ", outboundGrossCubage=" + outboundGrossCubage +
                ", outboundPackedCount=" + outboundPackedCount +
                ", lotNo0=" + lotNo0 +
                ", lotNo1=" + lotNo1 +
                ", lotNo2=" + lotNo2 +
                ", lotNo3=" + lotNo3 +
                ", lotNo4=" + lotNo4 +
                ", lotNo5=" + lotNo5 +
                ", lotNo6=" + lotNo6 +
                ", lotNo7=" + lotNo7 +
                ", lotNo8=" + lotNo8 +
                ", lotNo9=" + lotNo9 +
                ", status=" + status +
                ", remarks=" + remarks +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", destLocationProvince=" + destLocationProvince +
                ", destLocationCity=" + destLocationCity +
                ", destLocationCounty=" + destLocationCounty +
                ", destLocationDetail=" + destLocationDetail +
                "}";
    }
}
