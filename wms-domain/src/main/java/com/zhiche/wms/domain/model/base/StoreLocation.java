package com.zhiche.wms.domain.model.base;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 储位配置
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
@TableName("w_store_location")
public class StoreLocation extends Model<StoreLocation> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 仓库
     */
    @TableField("store_house_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;


    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String storeHouseName;

    /**
     * 库区
     */
    @TableField("store_area_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeAreaId;

    /**
     * 库区名称
     */
    @TableField(exist = false)
    private String storeAreaName;

    /**
     * 储位类型(10:普通储位,20:临时储位,30:虚拟库区,40:大储位)
     */
    private String type;
    /**
     * 储位编码
     */
    private String code;
    /**
     * 储位名称
     */
    private String name;
    /**
     * 最大存储量
     */
    @TableField("max_storage")
    private BigDecimal maxStorage;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 状态(10:正常,20:失效)
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField(exist = false)
    private BigDecimal storedQty;

    @TableField(exist = false)
    private BigDecimal usableQty;

    public StoreLocation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Long getStoreAreaId() {
        return storeAreaId;
    }

    public void setStoreAreaId(Long storeAreaId) {
        this.storeAreaId = storeAreaId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTypeByTypeName(String typeName) {
        if (StringUtils.isBlank(typeName)){
            this.type = "0";
            return;
        }
        switch (typeName) {
            case "普通库位":
                this.type = "10";
                break;
            case "临时库位":
                this.type = "20";
                break;
            case "虚拟库位":
                this.type = "30";
                break;
            case "大库位":
                this.type = "40";
                break;
            default:
                this.type = "0";
                break;
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMaxStorage() {
        return maxStorage;
    }

    public void setMaxStorage(BigDecimal maxStorage) {
        this.maxStorage = maxStorage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "StoreLocation{" +
                ", id=" + id +
                ", storeHouseId=" + storeHouseId +
                ", storeAreaId=" + storeAreaId +
                ", type=" + type +
                ", code=" + code +
                ", name=" + name +
                ", maxStorage=" + maxStorage +
                ", status=" + status +
                ", remark=" + remark +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "," + storedQty +
                "}";
    }

    public BigDecimal getStoredQty() {
        return storedQty;
    }

    public void setStoredQty(BigDecimal storedQty) {
        this.storedQty = storedQty;
    }

    public BigDecimal getUsableQty() {
        return usableQty;
    }

    public void setUsableQty(BigDecimal usableQty) {
        this.usableQty = usableQty;
    }

    public String getStoreHouseName() {
        return storeHouseName;
    }

    public void setStoreHouseName(String storeHoseName) {
        this.storeHouseName = storeHoseName;
    }

    public String getStoreAreaName() {
        return storeAreaName;
    }

    public void setStoreAreaName(String storeAreaName) {
        this.storeAreaName = storeAreaName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
