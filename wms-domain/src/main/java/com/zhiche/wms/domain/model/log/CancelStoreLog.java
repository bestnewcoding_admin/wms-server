package com.zhiche.wms.domain.model.log;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 退库记录表
 * @Date: Create in 10:52 2018/12/18
 */
@TableName("cancel_store_log")
public class CancelStoreLog extends Model<CancelStoreLog> {
    @Override
    protected Serializable pkVal () {
        return null;
    }

    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 通知单号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("notice_no")
    private String noticeNo;

    /**
     * 订单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;

    /**
     * 客户
     */
    @TableField("owner_id")
    private String ownerId;

    /**
     * 仓库
     */
    @TableField("houseName")
    private String houseName;

    /**
     * 车架号
     */
    @TableField("vin")
    private String vin;

    /**
     * 车型
     */
    @TableField("stan_vehicle_type")
    private String stanVehicleType;

    /**
     * 车型描述
     */
    @TableField("vehicle_describe")
    private String vehicleDescribe;

    /**
     * 备车线路
     */
    @TableField("prepare_drive_way")
    private String prepareDriveWay;

    /**
     * 出库详细id
     */
    @TableField("outbound_line_id")
    private Long outboundLineId;

    /**
     * 退库操作用户
     */
    @TableField("user_cancel")
    private String userCancel;

    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;

    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    /**
     * 领取状态（10：未领取，20：领取；30：完成）
     */
    @TableField("status_receive")
    private String statusReceive;
    /**
     * 退库领取任务用户
     */
    @TableField("user_receive")
    private String userReceive;
    /**
     * 仓库
     */
    @TableField("store_house_id")
    private String storeHouseId;
    /**
     * 退库状态
     */
    @TableField("status_outbound")
    private String statusOutbound;

    /**
     * 库区
     */
    @TableField(exist = false)
    private String locationName;
    /**
     * 库位
     */
    @TableField(exist = false)
    private String areaName;

    /**
     * 车架号
     */
    @TableField(exist = false)
    private String key;

    /**
     * 查询方式
     */
    @TableField(exist = false)
    private String visitType;

    /**
     * 查询方式
     */
    private String remark;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getNoticeNo () {
        return noticeNo;
    }

    public void setNoticeNo (String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getOwnerOrderNo () {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo (String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getOwnerId () {
        return ownerId;
    }

    public void setOwnerId (String ownerId) {
        this.ownerId = ownerId;
    }

    public String getHouseName () {
        return houseName;
    }

    public void setHouseName (String houseName) {
        this.houseName = houseName;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getStanVehicleType () {
        return stanVehicleType;
    }

    public void setStanVehicleType (String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    public String getVehicleDescribe () {
        return vehicleDescribe;
    }

    public void setVehicleDescribe (String vehicleDescribe) {
        this.vehicleDescribe = vehicleDescribe;
    }

    public String getPrepareDriveWay () {
        return prepareDriveWay;
    }

    public void setPrepareDriveWay (String prepareDriveWay) {
        this.prepareDriveWay = prepareDriveWay;
    }

    public Long getOutboundLineId () {
        return outboundLineId;
    }

    public void setOutboundLineId (Long outboundLineId) {
        this.outboundLineId = outboundLineId;
    }

    public String getUserCancel () {
        return userCancel;
    }

    public void setUserCancel (String userCancel) {
        this.userCancel = userCancel;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getStatusReceive () {
        return statusReceive;
    }

    public void setStatusReceive (String statusReceive) {
        this.statusReceive = statusReceive;
    }

    public String getUserReceive () {
        return userReceive;
    }

    public void setUserReceive (String userReceive) {
        this.userReceive = userReceive;
    }

    public String getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (String storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStatusOutbound () {
        return statusOutbound;
    }

    public void setStatusOutbound (String statusOutbound) {
        this.statusOutbound = statusOutbound;
    }

    public String getLocationName () {
        return locationName;
    }

    public void setLocationName (String locationName) {
        this.locationName = locationName;
    }

    public String getAreaName () {
        return areaName;
    }

    public void setAreaName (String areaName) {
        this.areaName = areaName;
    }

    public String getKey () {
        return key;
    }

    public void setKey (String key) {
        this.key = key;
    }

    public String getVisitType () {
        return visitType;
    }

    public void setVisitType (String visitType) {
        this.visitType = visitType;
    }

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("CancelStoreLog{");
        sb.append("id=").append(id);
        sb.append(", noticeNo='").append(noticeNo).append('\'');
        sb.append(", ownerOrderNo='").append(ownerOrderNo).append('\'');
        sb.append(", ownerId='").append(ownerId).append('\'');
        sb.append(", houseName='").append(houseName).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", stanVehicleType='").append(stanVehicleType).append('\'');
        sb.append(", vehicleDescribe='").append(vehicleDescribe).append('\'');
        sb.append(", prepareDriveWay='").append(prepareDriveWay).append('\'');
        sb.append(", outboundLineId=").append(outboundLineId);
        sb.append(", userCancel='").append(userCancel).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusReceive='").append(statusReceive).append('\'');
        sb.append(", userReceive='").append(userReceive).append('\'');
        sb.append(", storeHouseId='").append(storeHouseId).append('\'');
        sb.append(", statusOutbound='").append(statusOutbound).append('\'');
        sb.append(", locationName='").append(locationName).append('\'');
        sb.append(", areaName='").append(areaName).append('\'');
        sb.append(", key='").append(key).append('\'');
        sb.append(", visitType='").append(visitType).append('\'');
        sb.append(", remark='").append(remark).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
