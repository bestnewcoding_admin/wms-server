package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.inbound.InboundInspectLine;
import com.zhiche.wms.dto.inbound.InboundInspectDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 入库质检单明细 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
public interface InboundInspectLineMapper extends BaseMapper<InboundInspectLine> {

    List<InboundInspectDTO> queryInbound(Page<InboundInspectDTO> page, @Param("ew") Wrapper<InboundInspectDTO> ew);

    InboundInspectDTO getInInspectLine(@Param("id") Long id, @Param("houseId") Long houseId);

    Integer updateStatus(@Param("id") Long id, @Param("houseId") Long houseId, @Param("status") Integer status);

    void updateSQLMode();

    List<InboundInspectDTO> queryExportInspect(HashMap<String, Object> params);

    List<InboundInspectDTO> listInspectPage(Map<String, Object> params);

    int countInspectPage(HashMap<String, Object> params);
}
