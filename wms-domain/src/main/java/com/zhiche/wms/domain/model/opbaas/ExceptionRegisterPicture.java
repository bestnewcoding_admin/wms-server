package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 异常图片
 * </p>
 *
 * @author user
 * @since 2018-06-04
 */
@TableName("exception_register_picture")
public class ExceptionRegisterPicture extends Model<ExceptionRegisterPicture> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
    /**
     * 异常ID
     */
    @TableField("exception_id")
    private String exceptionId;
    /**
     * 图片KEY
     */
    @TableField("pic_key")
    private String picKey;
    /**
     * 图片URL
     */
    @TableField("pic_url")
    private String picUrl;
    /**
     * 存储服务器(qiniu:七牛)
     */
    @TableField("storage_server")
    private String storageServer;
    /**
     * 文件大小
     */
    private BigDecimal size;
    /**
     * 状态(10:正常,20:删除)
     */
    private String status;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(String exceptionId) {
        this.exceptionId = exceptionId;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getStorageServer() {
        return storageServer;
    }

    public void setStorageServer(String storageServer) {
        this.storageServer = storageServer;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExceptionRegisterPicture{" +
                ", id=" + id +
                ", exceptionId=" + exceptionId +
                ", picKey=" + picKey +
                ", picUrl=" + picUrl +
                ", storageServer=" + storageServer +
                ", size=" + size +
                ", status=" + status +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
