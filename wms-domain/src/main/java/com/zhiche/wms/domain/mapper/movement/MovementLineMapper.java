package com.zhiche.wms.domain.mapper.movement;

import com.zhiche.wms.domain.model.movement.MovementLine;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 移位明细 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface MovementLineMapper extends BaseMapper<MovementLine> {

}
