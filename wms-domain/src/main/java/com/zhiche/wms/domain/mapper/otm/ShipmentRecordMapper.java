package com.zhiche.wms.domain.mapper.otm;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentRecordDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:13 2018/12/21
 */
public interface ShipmentRecordMapper extends BaseMapper<ShipmentRecordDTO> {

    List<ShipmentRecordDTO> queryShipRecordList (Page<ShipmentRecordDTO> page, @Param("ew") EntityWrapper<ShipmentRecordDTO> ew);
}
