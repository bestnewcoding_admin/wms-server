package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegisterPicture;
import com.zhiche.wms.dto.opbaas.resultdto.PictureWithExcpDescDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 异常图片 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-06-04
 */
public interface ExceptionRegisterPictureMapper extends BaseMapper<ExceptionRegisterPicture> {

    List<PictureWithExcpDescDTO> listPicsAndDesc(@Param("ew") EntityWrapper<ExceptionRegisterPicture> picEW);
}
