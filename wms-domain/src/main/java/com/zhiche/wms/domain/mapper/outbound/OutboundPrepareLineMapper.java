package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareLine;
import com.zhiche.wms.dto.outbound.OutboundPrepareDTO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 出库备料单明细 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
public interface OutboundPrepareLineMapper extends BaseMapper<OutboundPrepareLine> {

    List<OutboundPrepareDTO> queryPage(Page<OutboundPrepareDTO> page, @Param("ew") Wrapper<OutboundPrepareDTO> ew);

    OutboundPrepareDTO getPrepare(@Param("id") Long id, @Param("houseId") Long houseId);

    OutboundPrepareDTO getPrepareBykey(@Param("key") String key, @Param("houseId") Long houseId);

    Integer updateStatus(@Param("id") Long id, @Param("houseId") Long houseId,
                         @Param("status") String status,
                         @Param("startTime")Date startTime,
                         @Param("preparator")String preparator);

    List<OutboundPrepareDTO> selectLinesWithHouse(@Param("ew") Wrapper<OutboundPrepareDTO> lineEW);
}
