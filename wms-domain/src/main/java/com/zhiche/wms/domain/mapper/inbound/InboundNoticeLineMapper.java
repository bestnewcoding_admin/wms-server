package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.dto.inbound.InboundDTO;
import com.zhiche.wms.dto.inbound.InboundNoticeDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 入库通知明细 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface InboundNoticeLineMapper extends BaseMapper<InboundNoticeLine> {

    /**
     * 根据头ID更新明细入库状态
     */
    Integer updateStatus(@Param("headerId") Long headerId);

    /**
     * 根据id号
     */
    InboundNoticeDTO getNoticeByLineId(@Param("id") Long id, @Param("houseId") Long houseId);

    /**
     * 根据二维码
     */
    List<InboundNoticeDTO> getNoticeByKey(@Param("key") String id, @Param("houseId") Long houseId);

    /**
     * 根据分页与Wrapper条件
     */
    List<InboundNoticeDTO> queryPageNotice(Page<InboundNoticeDTO> page, @Param("ew") Wrapper<InboundNoticeDTO> ew);

    List<InboundNoticeDTO> queryListNotice(@Param("ew") Wrapper<InboundNoticeDTO> ew);

    /**
     * 查询入库信息
     *
     * @param page 分页对象
     * @param ew   查询条件
     * @return 包含入库信息，质检信息及确认入库的结果
     */
    List<InboundDTO> queryInboundDetailPage(Page<InboundDTO> page, @Param("ew") Wrapper<InboundDTO> ew);

    List<InboundDTO> getInboundDetailByLotNo(@Param("vinNo") String id, @Param("houseId") Long houseId);

    InboundDTO getInboundDetailById(@Param("id") String id, @Param("houseId") Long houseId);

    List<InboundDTO> queryExportData(@Param("ew") EntityWrapper<InboundDTO> ew);

    List<InboundNoticeLine> pageInboundNotice(Page<InboundNoticeLine> page,@Param("ew") EntityWrapper<InboundNoticeLine> inEW);

    int selectCountWithHead(@Param("ew") EntityWrapper<InboundNoticeLine> noticeLineEntityWrapper);
}
