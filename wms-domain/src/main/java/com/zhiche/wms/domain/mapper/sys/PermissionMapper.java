package com.zhiche.wms.domain.mapper.sys;

import com.zhiche.wms.domain.model.sys.Permission;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 权限 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    /**
     * 通过用户得到权限
     * @param userId 用户ID
     * @param identity 标识，PC、APP
     * @returnc
     */
    List<Permission> selectPermListByUserId(@Param("userId") Integer userId,
                                            @Param("identity") String identity);
}
