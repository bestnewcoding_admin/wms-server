package com.zhiche.wms.domain.mapper.sys;

import com.zhiche.wms.domain.model.sys.RolePermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关联表 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
