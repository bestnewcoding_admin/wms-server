package com.zhiche.wms.dto.inbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zhiche.wms.domain.model.base.StoreLocation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StoreAreaAndLocationDTO implements Serializable {
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeAreaId;
    private String storeAreaName;
    private List<StoreLocation> storeLocationList;

    public Long getStoreAreaId() {
        return storeAreaId;
    }

    public void setStoreAreaId(Long storeAreaId) {
        this.storeAreaId = storeAreaId;
    }

    public String getStoreAreaName() {
        return storeAreaName;
    }

    public void setStoreAreaName(String storeAreaName) {
        this.storeAreaName = storeAreaName;
    }

    public List<StoreLocation> getStoreLocationList() {
        return storeLocationList;
    }

    public void setStoreLocationList(List<StoreLocation> storeLocationList) {
        this.storeLocationList = storeLocationList;
    }
    public void addStoreLocation(StoreLocation storeLocation) {
        if (Objects.isNull(storeLocationList))
            storeLocationList = new ArrayList<>();
        storeLocationList.add(storeLocation);
    }
}
