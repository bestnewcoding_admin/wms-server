package com.zhiche.wms.dto.opbaas.resultdto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel
public class TaskResultDTO implements Serializable {

    private String token;
    private String whCode;
    @ApiModelProperty(value = "用户编码")
    private String userCode;
    @ApiModelProperty(value = "起始地编码")
    private String originCode;
    @ApiModelProperty(value = "起始地名称")
    private String originName;
    @ApiModelProperty(value = "目的地编码")
    private String destCode;
    @ApiModelProperty(value = "目的地名称")
    private String destName;
    private String vin;
    private String orderNo;
    private String wayBillNo;
    private String taskType;
    private String taskStatus;
    private String orderReleaseGid;
    private String dispatchNo;
    private String taskId;
    private String vehicle;
    private String stanVehicleType;
    private String otmStatus;
    private String driverCode;
    private String driverName;
    private String driverPhone;
    private Date startTime;
    private Date finishTime;
    private String releaseId;
    private String qrCode;
    private String pointId;
    private String serviceProviderName;

    public String getServiceProviderName () {
        return serviceProviderName;
    }

    public void setServiceProviderName (String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    /**
     * 修改后的车型
     */
    private String modifiedVehicleType;

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getReleaseId() {
        return releaseId;
    }

    public void setReleaseId(String releaseId) {
        this.releaseId = releaseId;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getDispatchNo() {
        return dispatchNo;
    }

    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }

    public String getOrderReleaseGid() {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid(String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getDestCode() {
        return destCode;
    }

    public void setDestCode(String destCode) {
        this.destCode = destCode;
    }

    public String getDestName() {
        return destName;
    }

    public void setDestName(String destName) {
        this.destName = destName;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getWayBillNo() {
        return wayBillNo;
    }

    public void setWayBillNo(String wayBillNo) {
        this.wayBillNo = wayBillNo;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getStanVehicleType() {
        return stanVehicleType;
    }

    public void setStanVehicleType(String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    public String getOtmStatus() {
        return otmStatus;
    }

    public void setOtmStatus(String otmStatus) {
        this.otmStatus = otmStatus;
    }

    public String getDriverCode() {
        return driverCode;
    }

    public void setDriverCode(String driverCode) {
        this.driverCode = driverCode;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getModifiedVehicleType () {
        return modifiedVehicleType;
    }

    public void setModifiedVehicleType (String modifiedVehicleType) {
        this.modifiedVehicleType = modifiedVehicleType;
    }
}
