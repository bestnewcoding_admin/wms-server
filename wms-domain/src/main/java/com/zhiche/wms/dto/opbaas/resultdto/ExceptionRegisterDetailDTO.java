package com.zhiche.wms.dto.opbaas.resultdto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExceptionRegisterDetailDTO implements Serializable {

    private String level1Code;
    private String level1Name;
    private String level2Code;
    private String level2Name;
    private String level3Code;
    private String level3Sort;
    private String level3Name;
    private Integer countExcp = 0;
    private String vin;
    private String sort;
    private Date excpTime;

    //---异常详情图片表信息
    private String picId;
    private String exceptionId;
    private String storageServer;
    private BigDecimal size;
    private String picStatus;
    private String picKey;
    private String picUrl;
    private Date picGmtCreate;
    private Date picGmtModified;


    public String getLevel3Sort() {
        return level3Sort;
    }

    public void setLevel3Sort(String level3Sort) {
        this.level3Sort = level3Sort;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getPicStatus() {
        return picStatus;
    }

    public void setPicStatus(String picStatus) {
        this.picStatus = picStatus;
    }

    public Date getPicGmtCreate() {
        return picGmtCreate;
    }

    public void setPicGmtCreate(Date picGmtCreate) {
        this.picGmtCreate = picGmtCreate;
    }

    public Date getPicGmtModified() {
        return picGmtModified;
    }

    public void setPicGmtModified(Date picGmtModified) {
        this.picGmtModified = picGmtModified;
    }

    public String getPicId() {
        return picId;
    }

    public void setPicId(String picId) {
        this.picId = picId;
    }

    public String getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(String exceptionId) {
        this.exceptionId = exceptionId;
    }

    public String getStorageServer() {
        return storageServer;
    }

    public void setStorageServer(String storageServer) {
        this.storageServer = storageServer;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

    public String getLevel1Code() {
        return level1Code;
    }

    public void setLevel1Code(String level1Code) {
        this.level1Code = level1Code;
    }

    public String getLevel1Name() {
        return level1Name;
    }

    public void setLevel1Name(String level1Name) {
        this.level1Name = level1Name;
    }

    public String getLevel2Code() {
        return level2Code;
    }

    public void setLevel2Code(String level2Code) {
        this.level2Code = level2Code;
    }

    public String getLevel2Name() {
        return level2Name;
    }

    public void setLevel2Name(String level2Name) {
        this.level2Name = level2Name;
    }

    public String getLevel3Code() {
        return level3Code;
    }

    public void setLevel3Code(String level3Code) {
        this.level3Code = level3Code;
    }

    public String getLevel3Name() {
        return level3Name;
    }

    public void setLevel3Name(String level3Name) {
        this.level3Name = level3Name;
    }

    public Integer getCountExcp() {
        return countExcp;
    }

    public void setCountExcp(Integer countExcp) {
        this.countExcp = countExcp;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getExcpTime() {
        return excpTime;
    }

    public void setExcpTime(Date excpTime) {
        this.excpTime = excpTime;
    }
}
