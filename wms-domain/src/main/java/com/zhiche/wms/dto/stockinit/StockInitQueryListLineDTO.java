package com.zhiche.wms.dto.stockinit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class StockInitQueryListLineDTO implements Serializable {
    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long lineId;
    /**
     * 出库单头键
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long headerId;
    /**
     * 序号
     */
    private String seq;
    /**
     * 货主
     */
    private String ownerId;
    /**
     * 物料ID
     */
    private String materielId;
    /**
     * 物料代码
     */
    private String materielCode;
    /**
     * 物料名称
     */
    private String materielName;
    /**
     * 储位
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long locationId;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 净重
     */
    private BigDecimal netWeight;
    /**
     * 毛重
     */
    private BigDecimal grossWeight;
    /**
     * 体积
     */
    private BigDecimal grossCubage;
    /**
     * 件数
     */
    private BigDecimal packedCount;
    /**
     * 批号0
     */
    private String lotNo0;
    /**
     * 批号1
     */
    private String lotNo1;
    /**
     * 批号2
     */
    private String lotNo2;
    /**
     * 批号3
     */
    private String lotNo3;
    /**
     * 批号4
     */
    private String lotNo4;
    /**
     * 批号5
     */
    private String lotNo5;
    /**
     * 批号6
     */
    private String lotNo6;
    /**
     * 批号7
     */
    private String lotNo7;
    /**
     * 批号8
     */
    private String lotNo8;
    /**
     * 批号9
     */
    private String lotNo9;
    /**
     * 备注
     */
    private String lineRemarks;
    /**
     * 创建时间
     */
    private Date lineCreate;
    /**
     * 修改时间
     */
    private Date lineModified;

    private String houseCode;
    private String houseName;
    private String locationCode;
    private String locationName;

    public String getHouseCode() {
        return houseCode;
    }

    public void setHouseCode(String houseCode) {
        this.houseCode = houseCode;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Long getLineId() {
        return lineId;
    }

    public void setLineId(Long lineId) {
        this.lineId = lineId;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getGrossCubage() {
        return grossCubage;
    }

    public void setGrossCubage(BigDecimal grossCubage) {
        this.grossCubage = grossCubage;
    }

    public BigDecimal getPackedCount() {
        return packedCount;
    }

    public void setPackedCount(BigDecimal packedCount) {
        this.packedCount = packedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getLineRemarks() {
        return lineRemarks;
    }

    public void setLineRemarks(String lineRemarks) {
        this.lineRemarks = lineRemarks;
    }

    public Date getLineCreate() {
        return lineCreate;
    }

    public void setLineCreate(Date lineCreate) {
        this.lineCreate = lineCreate;
    }

    public Date getLineModified() {
        return lineModified;
    }

    public void setLineModified(Date lineModified) {
        this.lineModified = lineModified;
    }
}
