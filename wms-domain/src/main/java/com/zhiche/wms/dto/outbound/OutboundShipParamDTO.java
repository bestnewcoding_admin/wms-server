package com.zhiche.wms.dto.outbound;

import com.zhiche.wms.dto.base.PageVo;

import java.io.Serializable;

public class OutboundShipParamDTO extends PageVo implements Serializable {

    private String lotNo1;
    private String prepareNo;
    private String prepareStatus;
    private String houseId;
    private String noticeLineIds;
    private String qrCode;
    private String noticeLineId;
    private String areaId;
    private String locationId;
    private String noticeHeadSourceNo;
    private String noticeLineStatus;
    private String noticeHeadNo;
    private String ownerOrderNo;
    private String remark;

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getNoticeHeadNo() {
        return noticeHeadNo;
    }

    public void setNoticeHeadNo(String noticeHeadNo) {
        this.noticeHeadNo = noticeHeadNo;
    }

    public String getNoticeLineStatus() {
        return noticeLineStatus;
    }

    public void setNoticeLineStatus(String noticeLineStatus) {
        this.noticeLineStatus = noticeLineStatus;
    }

    public String getNoticeHeadSourceNo() {
        return noticeHeadSourceNo;
    }

    public void setNoticeHeadSourceNo(String noticeHeadSourceNo) {
        this.noticeHeadSourceNo = noticeHeadSourceNo;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(String noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getNoticeLineIds() {
        return noticeLineIds;
    }

    public void setNoticeLineIds(String noticeLineId) {
        this.noticeLineIds = noticeLineId;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getPrepareNo() {
        return prepareNo;
    }

    public void setPrepareNo(String prepareNo) {
        this.prepareNo = prepareNo;
    }

    public String getPrepareStatus() {
        return prepareStatus;
    }

    public void setPrepareStatus(String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }
}
