package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class OutboundNoticeParamDTO implements Serializable {

    private String detailLines;

    private List<OutboundNoticeLineParamDTO> lines;

    /**
     * 仓储订单ID
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long wmsOrderId;
    /**
     * 发货仓库
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 入库通知单号
     */
    private String noticeNo;
    /**
     * 货主
     */
    private String ownerId;

    /**
     * 货主单号
     */
    private String ownerOrderNo;
    /**
     * 下单日期
     */
    private Date orderDate;
    /**
     * 供应商
     */
    private String carrierId;
    /**
     * 供应商名称
     */
    private String carrierName;
    /**
     * 运输方式
     */
    private String transportMethod;
    /**
     * 车船号
     */
    private String plateNumber;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 司机联系方式
     */
    private String driverPhone;
    /**
     * 预计到货日期
     */
    private Date expectShipDateLower;
    /**
     * 预计收货时间上限
     */
    private Date expectShipDateUpper;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计入库数量
     */
    private BigDecimal expectSumQty;
    /**
     * 已入库数量
     */
    private BigDecimal outboundSumQty;
    /**
     * 明细行数
     */
    private Integer lineCount;
    /**
     * 资料生成方式(10:手工创建,20:oms系统触发,30:tms系统触发,40:君马系统触发)
     */
    private String genMethod;
    /**
     * 状态(10:未出库,20:部分出库,30:全部出库,40:关闭出库,50:取消)
     */
    private String status;
    /**
     * 来源唯一键
     */
    private String sourceKey;
    /**
     * 来源单据号
     */
    private String sourceNo;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    private String userCreate;
    /**
     * 修改人
     */
    private String userModified;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;



    public List<OutboundNoticeLineParamDTO> getLines() {
        return lines;
    }

    public void setLines(List<OutboundNoticeLineParamDTO> lines) {
        this.lines = lines;
    }

    public String getDetailLines() {
        return detailLines;
    }

    public void setDetailLines(String detailLines) {
        this.detailLines = detailLines;
    }

    public Long getWmsOrderId() {
        return wmsOrderId;
    }

    public void setWmsOrderId(Long wmsOrderId) {
        this.wmsOrderId = wmsOrderId;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getExpectShipDateLower() {
        return expectShipDateLower;
    }

    public void setExpectShipDateLower(Date expectShipDateLower) {
        this.expectShipDateLower = expectShipDateLower;
    }

    public Date getExpectShipDateUpper() {
        return expectShipDateUpper;
    }

    public void setExpectShipDateUpper(Date expectShipDateUpper) {
        this.expectShipDateUpper = expectShipDateUpper;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectSumQty() {
        return expectSumQty;
    }

    public void setExpectSumQty(BigDecimal expectSumQty) {
        this.expectSumQty = expectSumQty;
    }

    public BigDecimal getOutboundSumQty() {
        return outboundSumQty;
    }

    public void setOutboundSumQty(BigDecimal outboundSumQty) {
        this.outboundSumQty = outboundSumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getGenMethod() {
        return genMethod;
    }

    public void setGenMethod(String genMethod) {
        this.genMethod = genMethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}
