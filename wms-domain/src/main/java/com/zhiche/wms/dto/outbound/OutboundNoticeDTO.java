package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;

import java.io.Serializable;
import java.util.Date;

public class OutboundNoticeDTO extends OutboundNoticeLine implements Serializable {
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    private String storeHouseName;
    private String ownerName;
    private String carrierId;
    private String carrierName;
    private String plateNumber;
    private Date recvDate;
    private String noticeNo;
    private String isCanSend;

    public String getIsCanSend() {
        return isCanSend;
    }

    public void setIsCanSend(String isCanSend) {
        this.isCanSend = isCanSend;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName() {
        return storeHouseName;
    }

    public void setStoreHouseName(String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Date getRecvDate() {
        return recvDate;
    }

    public void setRecvDate(Date recvDate) {
        this.recvDate = recvDate;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }
}
