package com.zhiche.wms.dto.opbaas.resultdto;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 单个运单（单车）发运
 * @Date: Create in 15:15 2019/1/24
 */
public class OtmReleaseForShipDTO implements Serializable {

    private Long id;
    private String vin;
    private String status;
    private String releaseGid;
    private String shipmentGid;
    private String qrCode;
    private String customerId;
    private String cusOrderNo;
    private String cusWaybillNo;
    private String originLocationProvince;
    private String originLocationCity;
    private String originLocationCounty;
    private String originLocationAddress;
    private String originLocationGid;
    private String originLocationName;
    private String destLocationProvince;
    private String destLocationCity;
    private String destLocationCounty;
    private String destLocationAddress;
    private String destLocationGid;
    private String destLocationName;
    private String cusVehicleType;
    private String stanVehicleType;
    private Date gmtModify;
    private String isShip;
    private String userCode;

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("OtmReleaseForShipDTO{");
        sb.append("id=").append(id);
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", qrCode='").append(qrCode).append('\'');
        sb.append(", customerId='").append(customerId).append('\'');
        sb.append(", cusOrderNo='").append(cusOrderNo).append('\'');
        sb.append(", cusWaybillNo='").append(cusWaybillNo).append('\'');
        sb.append(", originLocationProvince='").append(originLocationProvince).append('\'');
        sb.append(", originLocationCity='").append(originLocationCity).append('\'');
        sb.append(", originLocationCounty='").append(originLocationCounty).append('\'');
        sb.append(", originLocationAddress='").append(originLocationAddress).append('\'');
        sb.append(", originLocationGid='").append(originLocationGid).append('\'');
        sb.append(", originLocationName='").append(originLocationName).append('\'');
        sb.append(", destLocationProvince='").append(destLocationProvince).append('\'');
        sb.append(", destLocationCity='").append(destLocationCity).append('\'');
        sb.append(", destLocationCounty='").append(destLocationCounty).append('\'');
        sb.append(", destLocationAddress='").append(destLocationAddress).append('\'');
        sb.append(", destLocationGid='").append(destLocationGid).append('\'');
        sb.append(", destLocationName='").append(destLocationName).append('\'');
        sb.append(", cusVehicleType='").append(cusVehicleType).append('\'');
        sb.append(", stanVehicleType='").append(stanVehicleType).append('\'');
        sb.append(", gmtModify=").append(gmtModify);
        sb.append(", isShip='").append(isShip).append('\'');
        sb.append(", userCode='").append(userCode).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getQrCode () {
        return qrCode;
    }

    public void setQrCode (String qrCode) {
        this.qrCode = qrCode;
    }

    public String getCustomerId () {
        return customerId;
    }

    public void setCustomerId (String customerId) {
        this.customerId = customerId;
    }

    public String getCusOrderNo () {
        return cusOrderNo;
    }

    public void setCusOrderNo (String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybillNo () {
        return cusWaybillNo;
    }

    public void setCusWaybillNo (String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getOriginLocationProvince () {
        return originLocationProvince;
    }

    public void setOriginLocationProvince (String originLocationProvince) {
        this.originLocationProvince = originLocationProvince;
    }

    public String getOriginLocationCity () {
        return originLocationCity;
    }

    public void setOriginLocationCity (String originLocationCity) {
        this.originLocationCity = originLocationCity;
    }

    public String getOriginLocationCounty () {
        return originLocationCounty;
    }

    public void setOriginLocationCounty (String originLocationCounty) {
        this.originLocationCounty = originLocationCounty;
    }

    public String getOriginLocationAddress () {
        return originLocationAddress;
    }

    public void setOriginLocationAddress (String originLocationAddress) {
        this.originLocationAddress = originLocationAddress;
    }

    public String getOriginLocationGid () {
        return originLocationGid;
    }

    public void setOriginLocationGid (String originLocationGid) {
        this.originLocationGid = originLocationGid;
    }

    public String getOriginLocationName () {
        return originLocationName;
    }

    public void setOriginLocationName (String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getDestLocationProvince () {
        return destLocationProvince;
    }

    public void setDestLocationProvince (String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity () {
        return destLocationCity;
    }

    public void setDestLocationCity (String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty () {
        return destLocationCounty;
    }

    public void setDestLocationCounty (String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationAddress () {
        return destLocationAddress;
    }

    public void setDestLocationAddress (String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getDestLocationGid () {
        return destLocationGid;
    }

    public void setDestLocationGid (String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getDestLocationName () {
        return destLocationName;
    }

    public void setDestLocationName (String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getCusVehicleType () {
        return cusVehicleType;
    }

    public void setCusVehicleType (String cusVehicleType) {
        this.cusVehicleType = cusVehicleType;
    }

    public String getStanVehicleType () {
        return stanVehicleType;
    }

    public void setStanVehicleType (String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    public Date getGmtModify () {
        return gmtModify;
    }

    public void setGmtModify (Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public String getIsShip () {
        return isShip;
    }

    public void setIsShip (String isShip) {
        this.isShip = isShip;
    }

    public String getUserCode () {
        return userCode;
    }

    public void setUserCode (String userCode) {
        this.userCode = userCode;
    }
}
